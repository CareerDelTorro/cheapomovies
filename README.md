# Cheap-O-Movies Project (http://40.113.207.203/)
## Goal
We are given 2 APIs for 2 popular movie databases and we are required to create a web app to allow customers get the cheapest price for movies from these two providers in a timely manner.
## Assumptions
* In the absence of a unique movie identifier (like ISAN) we assume that both databases are consistent with the exact titles of the movies and use them as movie IDs.
* In addition to the previous point, we are making an assumption that all supplementary information about movies such as date of release, description, image, etc. are somewhat consistent across databases as well. This helps us display supplementary information about the movies without worrying about possible conflicts (such as different dates of release).
* Despite the APIs for movie databases looking similar, in order to increase generality and reusability of the solution they will be treated as non-homogenous.
* In the absence of external APIs documentation, we will assume the structure of their data through inspection.
* We make an assumption that movies' ticket prices are not highly volatile and, therefore, can be cached and still be reliably delivered to the end user. This is underpinned by the fact that this service does not offer purchasing service and, therefore, up-to-date information will always be delivered when the users go to the relevant provider.
## Proposed Architecture
![Proposed Architecture Image](https://bitbucket.org/CareerDelTorro/cheapomovies/raw/8be17a514bf55efe671481717bb396453a6bca3f/docs/architecture.png)
## Wireframe Guidelines
![Wireframe 1](https://bitbucket.org/CareerDelTorro/cheapomovies/raw/8be17a514bf55efe671481717bb396453a6bca3f/docs/wireframe_1.png)
![Wirefram 2](https://bitbucket.org/CareerDelTorro/cheapomovies/raw/8be17a514bf55efe671481717bb396453a6bca3f/docs/wireframe_2.png)
## Result
In the end we produced a web app that performs periodic updates to the database and serves the content to the users. This ensures the fastest delivery of the content and, in light of our assumption, gives accurate results most of the time. An alternative version of the app which shifts this trade-off towards supplying real-time data at the expense of longer waiting time is currently in development.  
Link to the page: http://40.113.207.203/  
Of course, there is (was and will be) a need for further refinements. Here is a list of some of the major ones.
### Future Refinements
* massive harvesters - refactoring into smaller, testable components;
* addition of Typescript to the React front-end;
* implementing mechanisms to account for removal of movies from the external API (database "cache-busting")
* separating AggregatedMovie model into more modular components such as Movie, Provider and Movie-Provider;
* implementing logic of merging descriptive movie information from different providers into single entity.