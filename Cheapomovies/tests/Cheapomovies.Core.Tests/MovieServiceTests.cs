using System;
using System.Collections.Generic;
using System.Threading.Tasks;

using Cheapomovies.Core.Exceptions;
using Cheapomovies.Core.Services;
using Cheapomovies.Data;
using Cheapomovies.Entities.Models.Aggregated;

using Microsoft.EntityFrameworkCore;
using Moq;
using Xunit;

namespace Cheapomovies.Core.Tests
{
    /// <summary>
    /// Tests database-related methods of the MovieService.
    /// </summary>
    // NB: use separate context instances to set up and verify the methods
    public class MovieServiceTests
    {
        [Fact]
        public async Task Given_NoException_When_GetMovieTitleIdsAsync_Then_Success_Async()
        {
            DbContextOptions<CheapomoviesDbContext> dbContextOptions = new DbContextOptionsBuilder<CheapomoviesDbContext>()
                .UseInMemoryDatabase(databaseName: "gets_movie_title_ids_0")
                .Options;

            // ARRANGE
            const string MOVIE_TITLE_1 = "TestMovieTitle_1";
            const string MOVIE_TITLE_2 = "TestMovieTitle_2";
            const string PROVIDER_COMPOSITE_KEY_1 = "Provider_1";
            const string PROVIDER_COMPOSITE_KEY_2 = "Provider_2";

            using (CheapomoviesDbContext dbContext = new CheapomoviesDbContext(dbContextOptions))
            {
                dbContext.AggregatedMovie.Add(new AggregatedMovie
                {
                    TitleId = MOVIE_TITLE_1,
                    Provider = PROVIDER_COMPOSITE_KEY_1
                });
                dbContext.AggregatedMovie.Add(new AggregatedMovie
                {
                    TitleId = MOVIE_TITLE_2,
                    Provider = PROVIDER_COMPOSITE_KEY_2
                });
                dbContext.SaveChanges();
            }

            using (CheapomoviesDbContext dbContext = new CheapomoviesDbContext(dbContextOptions))
            {
                // ACT
                IMovieService movieService = new MovieService(dbContext);

                List<string> movieTitleIds =
                    await movieService.GetMovieTitleIdsAsync();

                // ASSERT whether there are exactly two elements with the requested title
                Assert.Collection(movieTitleIds,
                    (titleId) => Assert.Equal(MOVIE_TITLE_1, titleId),
                    (titleId) => Assert.Equal(MOVIE_TITLE_2, titleId)
                );
            }
        }

        [Fact]
        public async Task Given_Exception_When_GetMovieTitleIdsAsync_Then_Failure_Async()
        {
            DbContextOptions<CheapomoviesDbContext> dbContextOptions = new DbContextOptionsBuilder<CheapomoviesDbContext>()
                .UseInMemoryDatabase(databaseName: "gets_movie_title_ids_1")
                .Options;

            // ARRANGE - set up database as if everything is okay
            const string SAUGHT_MOVIE_TITLE = "TestMovieTitle";
            const string PROVIDER_COMPOSITE_KEY = "Provider";

            using (CheapomoviesDbContext dbContext = new CheapomoviesDbContext(dbContextOptions))
            {
                dbContext.AggregatedMovie.Add(new AggregatedMovie
                {
                    TitleId = SAUGHT_MOVIE_TITLE,
                    Provider = PROVIDER_COMPOSITE_KEY
                });
                dbContext.SaveChanges();
            }

            // rig the call to AggregateMovie model to throw an exception
            Mock<CheapomoviesDbContext> mockDbContext = new Mock<CheapomoviesDbContext>(dbContextOptions);
            mockDbContext.Setup((dbContext) => dbContext.AggregatedMovie).Throws<Exception>();

            // ACT - query the database as if everything was okay
            IMovieService movieService = new MovieService(mockDbContext.Object);

            // ASSERT
            await Assert.ThrowsAsync<DatabaseOperationException>(async () => await movieService.GetAggregatedMoviesByTitleAsync(SAUGHT_MOVIE_TITLE));
        }

        [Fact]
        public async Task Given_CorrectTitle_When_GetAggregatedMoviesByTitleAsync_Then_Success_Async()
        {
            DbContextOptions<CheapomoviesDbContext> dbContextOptions = new DbContextOptionsBuilder<CheapomoviesDbContext>()
                .UseInMemoryDatabase(databaseName: "gets_aggregated_movies_0")
                .Options;

            // ARRANGE
            const string SAUGHT_MOVIE_TITLE = "TestMovieTitle";
            const string DIFFERENT_MOVIE_TITLE = "TestMovieTitleDifferent";
            const string PROVIDER_COMPOSITE_KEY_1 = "Provider_1";
            const string PROVIDER_COMPOSITE_KEY_2 = "Provider_2";

            using (CheapomoviesDbContext dbContext = new CheapomoviesDbContext(dbContextOptions))
            {
                dbContext.AggregatedMovie.Add(new AggregatedMovie
                {
                    TitleId = SAUGHT_MOVIE_TITLE,
                    Provider = PROVIDER_COMPOSITE_KEY_1
                });
                dbContext.AggregatedMovie.Add(new AggregatedMovie
                {
                    TitleId = SAUGHT_MOVIE_TITLE,
                    Provider = PROVIDER_COMPOSITE_KEY_2
                });
                dbContext.AggregatedMovie.Add(new AggregatedMovie
                {
                    TitleId = DIFFERENT_MOVIE_TITLE,
                    Provider = PROVIDER_COMPOSITE_KEY_1
                });
                dbContext.SaveChanges();
            }

            using (CheapomoviesDbContext dbContext = new CheapomoviesDbContext(dbContextOptions))
            {
                // ACT
                IMovieService movieService = new MovieService(dbContext);

                List<AggregatedMovie> aggregatedMoviesResult =
                    await movieService.GetAggregatedMoviesByTitleAsync(SAUGHT_MOVIE_TITLE);

                // ASSERT whether there are exactly two elements with the requested title
                Assert.Collection(aggregatedMoviesResult,
                    (aggregatedMovie) => Assert.Equal(SAUGHT_MOVIE_TITLE, aggregatedMovie.TitleId),
                    (aggregatedMovie) => Assert.Equal(SAUGHT_MOVIE_TITLE, aggregatedMovie.TitleId)
                );
            }
        }

        [Fact]
        public async Task Given_WrongTitle_When_GetAggregatedMoviesByTitleAsync_Then_Empty_Async()
        {
            DbContextOptions<CheapomoviesDbContext> dbContextOptions = new DbContextOptionsBuilder<CheapomoviesDbContext>()
                .UseInMemoryDatabase(databaseName: "gets_aggregated_movies_1")
                .Options;

            // ARRANGE
            const string SAUGHT_MOVIE_TITLE = "TestMovieTitle";
            const string DIFFERENT_MOVIE_TITLE = "TestMovieTitleDifferent";
            const string PROVIDER_COMPOSITE_KEY = "Provider";

            using (CheapomoviesDbContext dbContext = new CheapomoviesDbContext(dbContextOptions))
            {
                dbContext.AggregatedMovie.Add(new AggregatedMovie
                {
                    TitleId = DIFFERENT_MOVIE_TITLE,
                    Provider = PROVIDER_COMPOSITE_KEY
                });
                dbContext.SaveChanges();
            }

            using (CheapomoviesDbContext dbContext = new CheapomoviesDbContext(dbContextOptions))
            {
                // ACT
                IMovieService movieService = new MovieService(dbContext);

                List<AggregatedMovie> aggregatedMoviesResult =
                    await movieService.GetAggregatedMoviesByTitleAsync(SAUGHT_MOVIE_TITLE);

                // ASSERT
                Assert.Empty(aggregatedMoviesResult);
            }
        }

        [Fact]
        public async Task Given_Exception_When_GetAggregatedMoviesByTitleAsync_Then_Failure_Async()
        {
            DbContextOptions<CheapomoviesDbContext> dbContextOptions = new DbContextOptionsBuilder<CheapomoviesDbContext>()
                .UseInMemoryDatabase(databaseName: "gets_aggregated_movies_2")
                .Options;

            // ARRANGE - set up database as if everything is okay
            const string SAUGHT_MOVIE_TITLE = "TestMovieTitle";
            const string PROVIDER_COMPOSITE_KEY = "Provider";

            using (CheapomoviesDbContext dbContext = new CheapomoviesDbContext(dbContextOptions))
            {
                dbContext.AggregatedMovie.Add(new AggregatedMovie
                {
                    TitleId = SAUGHT_MOVIE_TITLE,
                    Provider = PROVIDER_COMPOSITE_KEY
                });
                dbContext.SaveChanges();
            }

            // rig the call to AggregateMovie model to throw an exception
            Mock<CheapomoviesDbContext> mockDbContext = new Mock<CheapomoviesDbContext>(dbContextOptions);
            mockDbContext.Setup((dbContext) => dbContext.AggregatedMovie).Throws<Exception>();

            // ACT - query the database as if everything was okay
            IMovieService movieService = new MovieService(mockDbContext.Object);

            // ASSERT
            await Assert.ThrowsAsync<DatabaseOperationException>(async () => await movieService.GetAggregatedMoviesByTitleAsync(SAUGHT_MOVIE_TITLE));
        }
    }
}