using System;
using System.Collections.Generic;
using System.Threading.Tasks;

using Cheapomovies.Controllers;
using Cheapomovies.Core.Services;
using Cheapomovies.Entities.Models.Aggregated;

using Microsoft.AspNetCore.Mvc;
using Moq;
using Xunit;

namespace Cheapomovies.Web.Tests
{
    /// <summary>
    /// Tests whether the MoviesController corresponds with expected actions
    /// after calling underlying services.
    /// </summary>
    public class MoviesControllerTests
    {
        [Fact]
        public async Task Given_NoException_When_GetMovieTitlesAsync_Then_Success_Async()
        {
            // ARRANGE
            string MOCK_ID_1 = "id_1";
            string MOCK_ID_2 = "id_2";

            Mock<IMovieService> mockMovieService = new Mock<IMovieService>();

            mockMovieService.Setup((movieService) => movieService.GetMovieTitleIdsAsync())
                .ReturnsAsync(new List<string> { MOCK_ID_1, MOCK_ID_2 });

            // ACT
            MoviesController moviesController = new MoviesController(mockMovieService.Object);

            List<string> idsListResult =
                (await moviesController.GetMovieTitlesAsync()).Value;

            // ASSERT
            Assert.Collection(idsListResult,
                (id) => Assert.Equal(MOCK_ID_1, id),
                (id) => Assert.Equal(MOCK_ID_2, id)
            );
        }

        [Fact]
        public async Task Given_Exception_When_GetMovieTitlesAsync_Then_Failure_Async()
        {
            // ARRANGE
            Mock<IMovieService> mockMovieService = new Mock<IMovieService>();

            mockMovieService.Setup((movieService) => movieService.GetMovieTitleIdsAsync())
                .Throws<Exception>();

            // ACT
            MoviesController moviesController = new MoviesController(mockMovieService.Object);

            StatusCodeResult httpStatusCode =
                (await moviesController.GetMovieTitlesAsync()).Result as StatusCodeResult;

            // ASSERT
            Assert.Equal(500, httpStatusCode.StatusCode);
        }

        [Fact]
        public async Task Given_CorrectTitle_When_GetAggregatedMoviesAsync_Then_Success_Async()
        {
            // ARRANGE
            const string CORRECT_MOVIE_TITLE = "CorrectMovieTitle";

            Mock<IMovieService> mockMovieService = new Mock<IMovieService>();

            mockMovieService.Setup((movieService) => movieService.GetAggregatedMoviesByTitleAsync(CORRECT_MOVIE_TITLE))
                .ReturnsAsync(new List<AggregatedMovie>
                    {
                        new AggregatedMovie
                        {
                            TitleId = CORRECT_MOVIE_TITLE
                        },
                        new AggregatedMovie
                        {
                            TitleId = CORRECT_MOVIE_TITLE
                        }
                    }
                );

            // ACT
            MoviesController moviesController = new MoviesController(mockMovieService.Object);

            List<AggregatedMovie> aggregatedMoviesResult = 
                (await moviesController.GetAggregatedMoviesAsync(CORRECT_MOVIE_TITLE)).Value;

            // ASSERT
            Assert.Collection(aggregatedMoviesResult,
                (aggregatedMovie) => Assert.Equal(CORRECT_MOVIE_TITLE, aggregatedMovie.TitleId),
                (aggregatedMovie) => Assert.Equal(CORRECT_MOVIE_TITLE, aggregatedMovie.TitleId)
            );
        }

        [Fact]
        public async Task Given_WrongTitle_When_GetAggregatedMoviesAsync_Then_Empty_Async()
        {
            // ARRANGE
            const string WRONG_MOVIE_TITLE = "WrongMovieTitle";

            Mock<IMovieService> mockMovieService = new Mock<IMovieService>();

            mockMovieService.Setup((movieService) => movieService.GetAggregatedMoviesByTitleAsync(WRONG_MOVIE_TITLE))
                .ReturnsAsync(new List<AggregatedMovie>());

            // ACT
            MoviesController moviesController = new MoviesController(mockMovieService.Object);

            List<AggregatedMovie> aggregatedMoviesResult =
                (await moviesController.GetAggregatedMoviesAsync(WRONG_MOVIE_TITLE)).Value;

            // ASSERT
            Assert.Empty(aggregatedMoviesResult);
        }

        [Fact]
        public async Task Given_Exception_When_GetAggregatedMoviesAsync_Then_Failure_Async()
        {
            // ARRANGE
            Mock<IMovieService> mockMovieService = new Mock<IMovieService>();

            mockMovieService.Setup((movieService) => movieService.GetAggregatedMoviesByTitleAsync(It.IsAny<string>()))
                .Throws<Exception>();

            // ACT
            MoviesController moviesController = new MoviesController(mockMovieService.Object);

            StatusCodeResult httpStatusCode =
                (await moviesController.GetAggregatedMoviesAsync(It.IsAny<string>())).Result as StatusCodeResult;

            // ASSERT
            Assert.Equal(500, httpStatusCode.StatusCode);
        }
    }

}
