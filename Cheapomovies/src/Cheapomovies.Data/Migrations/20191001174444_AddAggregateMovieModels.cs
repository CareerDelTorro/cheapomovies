﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Cheapomovies.Data.Migrations
{
    public partial class AddAggregateMovieModels : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_AggregatedMovie",
                table: "AggregatedMovie");

            migrationBuilder.DropColumn(
                name: "Title",
                table: "AggregatedMovie");

            migrationBuilder.AlterColumn<string>(
                name: "Provider",
                table: "AggregatedMovie",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AddColumn<string>(
                name: "TitleId",
                table: "AggregatedMovie",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<double>(
                name: "Price",
                table: "AggregatedMovie",
                nullable: false,
                defaultValue: 0.0);

            migrationBuilder.AddUniqueConstraint(
                name: "AK_AggregatedMovie_Provider_TitleId",
                table: "AggregatedMovie",
                columns: new[] { "Provider", "TitleId" });

            migrationBuilder.AddPrimaryKey(
                name: "PK_AggregatedMovie",
                table: "AggregatedMovie",
                columns: new[] { "TitleId", "Provider" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropUniqueConstraint(
                name: "AK_AggregatedMovie_Provider_TitleId",
                table: "AggregatedMovie");

            migrationBuilder.DropPrimaryKey(
                name: "PK_AggregatedMovie",
                table: "AggregatedMovie");

            migrationBuilder.DropColumn(
                name: "TitleId",
                table: "AggregatedMovie");

            migrationBuilder.DropColumn(
                name: "Price",
                table: "AggregatedMovie");

            migrationBuilder.AlterColumn<string>(
                name: "Provider",
                table: "AggregatedMovie",
                nullable: true,
                oldClrType: typeof(string));

            migrationBuilder.AddColumn<string>(
                name: "Title",
                table: "AggregatedMovie",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddPrimaryKey(
                name: "PK_AggregatedMovie",
                table: "AggregatedMovie",
                column: "Title");
        }
    }
}
