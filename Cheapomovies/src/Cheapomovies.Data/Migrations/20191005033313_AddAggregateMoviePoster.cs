﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Cheapomovies.Data.Migrations
{
    public partial class AddAggregateMoviePoster : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Poster",
                table: "AggregatedMovie",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Poster",
                table: "AggregatedMovie");
        }
    }
}
