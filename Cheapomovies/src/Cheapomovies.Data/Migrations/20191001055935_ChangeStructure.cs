﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Cheapomovies.Data.Migrations
{
    public partial class ChangeStructure : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "CinemaWorldMovie");

            migrationBuilder.CreateTable(
                name: "CinemaWorldMovieFull",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false),
                    Type = table.Column<string>(nullable: true),
                    Title = table.Column<string>(nullable: true),
                    Year = table.Column<int>(nullable: false),
                    Price = table.Column<double>(nullable: false),
                    Rated = table.Column<string>(nullable: true),
                    Released = table.Column<string>(nullable: true),
                    Runtime = table.Column<string>(nullable: true),
                    Genre = table.Column<string>(nullable: true),
                    Director = table.Column<string>(nullable: true),
                    Writer = table.Column<string>(nullable: true),
                    Actors = table.Column<string>(nullable: true),
                    Plot = table.Column<string>(nullable: true),
                    Language = table.Column<string>(nullable: true),
                    Country = table.Column<string>(nullable: true),
                    Awards = table.Column<string>(nullable: true),
                    Poster = table.Column<string>(nullable: true),
                    Metascore = table.Column<double>(nullable: false),
                    Rating = table.Column<double>(nullable: false),
                    Votes = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CinemaWorldMovieFull", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "CinemaWorldMovieFull");

            migrationBuilder.CreateTable(
                name: "CinemaWorldMovie",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false),
                    Actors = table.Column<string>(nullable: true),
                    Awards = table.Column<string>(nullable: true),
                    Country = table.Column<string>(nullable: true),
                    Director = table.Column<string>(nullable: true),
                    Genre = table.Column<string>(nullable: true),
                    Language = table.Column<string>(nullable: true),
                    Metascore = table.Column<double>(nullable: false),
                    Plot = table.Column<string>(nullable: true),
                    Poster = table.Column<string>(nullable: true),
                    Price = table.Column<double>(nullable: false),
                    Rated = table.Column<string>(nullable: true),
                    Rating = table.Column<double>(nullable: false),
                    Released = table.Column<string>(nullable: true),
                    Runtime = table.Column<string>(nullable: true),
                    Title = table.Column<string>(nullable: true),
                    Type = table.Column<string>(nullable: true),
                    Votes = table.Column<int>(nullable: false),
                    Writer = table.Column<string>(nullable: true),
                    Year = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CinemaWorldMovie", x => x.Id);
                });
        }
    }
}
