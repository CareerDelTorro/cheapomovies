﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Cheapomovies.Data.Migrations
{
    public partial class AddAggregateMovieProperties : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Plot",
                table: "AggregatedMovie",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "Year",
                table: "AggregatedMovie",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Plot",
                table: "AggregatedMovie");

            migrationBuilder.DropColumn(
                name: "Year",
                table: "AggregatedMovie");
        }
    }
}
