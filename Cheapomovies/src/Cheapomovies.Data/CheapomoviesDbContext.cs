﻿using Cheapomovies.Entities.Models.Aggregated;
using Cheapomovies.Entities.Models.CinemaWorld;
using Cheapomovies.Entities.Models.FilmWorld;

using Microsoft.EntityFrameworkCore;

namespace Cheapomovies.Data
{
    public class CheapomoviesDbContext : DbContext
    {
        public DbSet<CinemaWorldMovieFull> CinemaWorldMovieFull { get; set; }

        public DbSet<FilmWorldMovieFull> FilmWorldMovieFull { get; set; }

        // declated virtual to allow for overridng during integration testing
        public virtual DbSet<AggregatedMovie> AggregatedMovie { get; set; }

        public CheapomoviesDbContext(DbContextOptions<CheapomoviesDbContext> options) : base(options) { }

        // composite primary key: titleId + provider
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<AggregatedMovie>()
                .HasKey(am => new { am.TitleId, am.Provider });
        }
    }
}