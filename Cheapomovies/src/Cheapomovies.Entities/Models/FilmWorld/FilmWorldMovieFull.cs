﻿using System.ComponentModel.DataAnnotations;

using Newtonsoft.Json;

namespace Cheapomovies.Entities.Models.FilmWorld
{
    [JsonObject]
    public class FilmWorldMovieFull
    {
        [Key]
        [JsonProperty("ID")]
        public string Id { get; set; }
        [JsonProperty("Type")]
        public string Type { get; set; }
        [JsonProperty("Title")]
        public string Title { get; set; }
        [JsonProperty("Year")]
        public int Year { get; set; }
        [JsonProperty("Price")]
        public double Price { get; set; }
        [JsonProperty("Rated")]
        public string Rated { get; set; }
        [JsonProperty("Released")]
        public string Released { get; set; }
        [JsonProperty("Runtime")]
        public string Runtime { get; set; }
        [JsonProperty("Genre")]
        public string Genre { get; set; }
        [JsonProperty("Director")]
        public string Director { get; set; }
        [JsonProperty("Writer")]
        public string Writer { get; set; }
        [JsonProperty("Actors")]
        public string Actors { get; set; }
        [JsonProperty("Plot")]
        public string Plot { get; set; }
        [JsonProperty("Language")]
        public string Language { get; set; }
        [JsonProperty("Country")]
        public string Country { get; set; }
        [JsonProperty("Poster")]
        public string Poster { get; set; }
        [JsonProperty("Metascore")]
        public double Metascore { get; set; }
        [JsonProperty("Rating")]
        public double Rating { get; set; }
        [JsonProperty("Votes")]
        public string Votes { get; set; }
    }
}
