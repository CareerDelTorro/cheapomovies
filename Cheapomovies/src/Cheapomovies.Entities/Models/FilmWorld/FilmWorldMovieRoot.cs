﻿using System.Collections.Generic;

using Newtonsoft.Json;

namespace Cheapomovies.Entities.Models.FilmWorld
{
    [JsonObject]
    public class FilmWorldMovieRoot
    {
        [JsonProperty("Movies")]
        public IEnumerable<FilmWorldMovieShort> Movies { get; set; }
    }
}
