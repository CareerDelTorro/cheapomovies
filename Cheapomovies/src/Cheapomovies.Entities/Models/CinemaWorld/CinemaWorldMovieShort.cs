﻿using Newtonsoft.Json;

namespace Cheapomovies.Entities.Models.CinemaWorld
{
    [JsonObject]
    public class CinemaWorldMovieShort
    {
        [JsonProperty("Title")]
        public string Title { get; set; }
        [JsonProperty("Year")]
        public int Year { get; set; }
        [JsonProperty("ID")]
        public string Id { get; set; }
        [JsonProperty("Type")]
        public string Type { get; set; }
        [JsonProperty("Poster")]
        public string Poster { get; set; }
    }
}
