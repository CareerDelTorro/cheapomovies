﻿using System.Collections.Generic;

using Newtonsoft.Json;

namespace Cheapomovies.Entities.Models.CinemaWorld
{
    [JsonObject]
    public class CinemaWorldMovieRoot
    {
        [JsonProperty("Movies")]
        public IEnumerable<CinemaWorldMovieShort> Movies { get; set; }
    }
}
