﻿using System.ComponentModel.DataAnnotations;

using Newtonsoft.Json;

namespace Cheapomovies.Entities.Models.Aggregated
{
    [JsonObject]
    public class AggregatedMovie
    {
        [Key]
        [JsonProperty("TitleId")]
        public string TitleId { get; set; }
        [Key]
        [JsonProperty("Provider")]
        public string Provider { get; set; }
        [JsonProperty("Price")]
        public double Price { get; set; }
        [JsonProperty("Year")]
        public int Year { get; set; }
        [JsonProperty("Plot")]
        public string Plot { get; set; }
        [JsonProperty("Poster")]
        public string Poster { get; set; }
    }
}
