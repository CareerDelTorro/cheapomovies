﻿import React from 'react';
import ReactDOM from 'react-dom';
import MovieInformationPanel from './MovieInformationPanel';

it('renders without crashing', () => {
    const div = document.createElement('div');
    ReactDOM.render(<MovieInformationPanel selectedMovieEntries={[]}/>, div);
});