﻿import React from 'react';

import MovieSelector from './../atoms/MovieSelector';
import OrderSelector from './../atoms/OrderSelector';

import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Button from 'react-bootstrap/Button';


/*
 * Displays components relevant to movie selection.
 */ 
class MovieSelectionPanel extends React.Component {
    render() {
        return (
            <Row className="justify-content-center">
                <Col xs="auto">
                    <MovieSelector
                        availableMovieTitles={this.props.availableMovieTitles}
                        selectedMovieTitle={this.props.selectedMovieTitle}
                        onMovieTitleChange={this.props.onMovieTitleChange}
                    />
                </Col>
                <Col xs="auto">
                    <OrderSelector
                        sortingOrder={this.props.sortingOrder}
                        onSortingOrderChange={this.props.onSortingOrderChange}
                    />
                </Col>
                <Col xs="auto" className="d-flex align-items-end">
                    <Button onClick={this.props.onRefresh} variant="info" className="mb-3">
                        Refresh available movies
                    </Button>
                </Col>
            </Row>
        );
    }
}
export default MovieSelectionPanel;