﻿import React from 'react';

import MovieInformationCard from './../atoms/MovieInformationCard';
import MovieEntriesTable from './../atoms/MovieEntriesTable';

import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';

/*
 * Displays components relevant to movie information.
 */
class MovieInformationPanel extends React.Component {
    render() {
        /* arbitrarily chosen movie information: randomly */
        const randomMovieInformationEntry =
            this.props.selectedMovieEntries.length > 0
                ? this.props.selectedMovieEntries[Math.floor(Math.random() * this.props.selectedMovieEntries.length)]
                : null;

        return (
            <Row className="justify-content-center mt-4">
                <Col xs={6}>
                    {this.props.selectedMovieEntries.length > 0 &&
                    <MovieInformationCard
                        movieObject={randomMovieInformationEntry}
                    />
                    }
                </Col>
                <Col xs={4}>
                    <MovieEntriesTable
                        selectedMovieEntries={this.props.selectedMovieEntries}
                        sortingOrder={this.props.sortingOrder}
                    />
                </Col>
            </Row>
        );
    }
}
export default MovieInformationPanel;