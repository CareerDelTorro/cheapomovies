﻿import React from 'react';
import ReactDOM from 'react-dom';
import MovieSelectionPanel from './MovieSelectionPanel';

it('renders without crashing', () => {
    const div = document.createElement('div');
    ReactDOM.render(<MovieSelectionPanel availableMovieTitles={[]}/>, div);
});