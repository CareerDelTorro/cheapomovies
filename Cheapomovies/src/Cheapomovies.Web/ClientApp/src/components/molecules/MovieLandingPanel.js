﻿import React from 'react';

import MovieInformationCard from './../atoms/MovieInformationCard';

import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';

/*
 * Displays components that appear on the front-page before user made any selection.
 */
class MovieLandingPanel extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            randomMovieEntries: []
        }
    }

    render() {
        /* arbitrarily chosen movie information: randomly */
        const randomMovieInformationEntry =
            this.state.randomMovieEntries.length > 0
                ? this.state.randomMovieEntries[Math.floor(Math.random() * this.state.randomMovieEntries.length)]
                : null;

        return (
            <Row className="justify-content-center mt-4 pt-4" >
                <Col xs={6} >
                    {this.state.randomMovieEntries.length > 0 &&
                        <>
                            <h4 className="text-center mb-3">
                                <i>Cheap-O-Movies' random selection!</i>
                            </h4>
                            <MovieInformationCard 
                                onClick={ this.props.onInformationCardClick.bind(this, randomMovieInformationEntry) }
                                clickable={"dummyString"}
                                movieObject={randomMovieInformationEntry}
                            />
                        </>
                    }
                </Col>
            </Row>
        );
    }

    componentDidMount() {
        /* arbitrarily chosen movie title - randomly */
        const movieTitles = this.props.availableMovieTitles;
        const randomMovieTitle =
            movieTitles[Math.floor(Math.random() * movieTitles.length)];
        // load movie information of randomly chosen movie
        this.fetchMovieEntries(randomMovieTitle);
    }

    fetchMovieEntries(randomMovieTitle) {
        // fetch all provider-movie entries for the given title
        fetch(("/api/movies/titles/" + randomMovieTitle), {
            headers: new Headers({
                'Content-Type': 'application/json'
            })
        })
        .then((response) => {
            if (response.ok) {
                return response.json();
            } else {
                console.log(response.status + " " + response.statusText);
                throw new Error("Something went wrong while fetching random movie entries!");
            }
        })
        .then((randomMovieEntries) => {
            console.log("Fetched random movie entries.");
            this.setState({
                randomMovieEntries: randomMovieEntries
            });
        })
        .catch((error) => {
            this.props.onError(error);
        });
    }
}
export default MovieLandingPanel;