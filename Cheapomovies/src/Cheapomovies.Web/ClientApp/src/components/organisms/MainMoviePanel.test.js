﻿import React from 'react';
import ReactDOM from 'react-dom';
import MainMoviePanel from './MainMoviePanel';

it('renders without crashing', () => {
    const div = document.createElement('div');
    ReactDOM.render(<MainMoviePanel />, div);
});