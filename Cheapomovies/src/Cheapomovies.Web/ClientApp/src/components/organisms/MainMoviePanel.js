﻿import React from 'react';

import MovieLandingPanel from './../molecules/MovieLandingPanel';
import MovieInformationPanel from './../molecules/MovieInformationPanel';
import MovieSelectionPanel from './../molecules/MovieSelectionPanel';

import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';

/*
 * Main logic component - fetches and holds movie-related information.
 */
class MainMoviePanel extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            availableMovieTitles: [],
            selectedMovieTitle: "None",
            selectedMovieEntries: [],
            sortingOrder: 1,
            error: null
        }
        this.handleErrors = this.handleErrors.bind(this);
        this.handleMovieTitleChange = this.handleMovieTitleChange.bind(this);
        this.handleSortingOrderChange = this.handleSortingOrderChange.bind(this);
        this.handleInformationCardClick = this.handleInformationCardClick.bind(this);
        this.handleRefresh = this.handleRefresh.bind(this);
    }

    render() {
        return (
            <>
                {this.state.error !== null &&
                // display an error if any exists
                <Row>
                    <Col>
                        <h5>
                            {this.state.error.message}
                            <br />
                            We apologise for the inconvenience :( Please refresh the page or try again later.
                        </h5>;
                    </Col>
                </Row>
                }

                <Row>
                    <Col>
                        <MovieSelectionPanel
                            availableMovieTitles={this.state.availableMovieTitles}
                            selectedMovieTitle={this.state.selectedMovieTitle}
                            sortingOrder={this.state.sortingOrder}
                            onMovieTitleChange={this.handleMovieTitleChange}
                            onSortingOrderChange={this.handleSortingOrderChange}
                            onRefresh={this.handleRefresh}
                        />
                    </Col>
                </Row>

                <Row>
                    <Col>
                        {this.state.selectedMovieTitle !== "None" &&
                        // display movie information if any chosen
                        <MovieInformationPanel
                            selectedMovieEntries={this.state.selectedMovieEntries}
                            sortingOrder={this.state.sortingOrder}
                        />
                        }
                        {this.state.availableMovieTitles.length > 0 &&
                            this.state.selectedMovieTitle === "None" &&
                        // or display a custom landing panel instead
                        <MovieLandingPanel
                            availableMovieTitles={this.state.availableMovieTitles}
                            onInformationCardClick={this.handleInformationCardClick}
                            onError={this.handleErrors}
                        />
                        }
                    </Col>
                </Row>
            </>
        );
    }

    componentDidMount() {
        // get movie titles to choose from
        this.fetchMovieTitles();
        // load default movie entries (if any movie title was selected by default)
        if (this.state.selectedMovieTitle !== "None") {
            this.fetchMovieEntries();
        }
    }

    componentDidUpdate(_, prevState) {
        // fetch new movie entries only for a different supplied movie title
        if (this.state.selectedMovieTitle !== prevState.selectedMovieTitle) {
            this.fetchMovieEntries();
        }
    }

    // fetch movie titles from the API
    fetchMovieTitles() {
        fetch("/api/movies/titles", {
            headers: new Headers({
                'Content-Type': 'application/json'
            })
        })
        .then((response) => {
            if (response.ok) {
                return response.json();
            } else {
                console.log(response.status + " " + response.statusText);
                throw new Error("Something went wrong while fetching movie titles!");
            }
        })
        .then((availableMovieTitles) => {
            this.setState({
                availableMovieTitles: availableMovieTitles
            });
            console.log("Fetched movie titles.");
        })
        .catch((error) => {
            this.handleErrors(error);
        });
    }

    fetchMovieEntries() {
        // fetch all provider-movie entries for the given title
        fetch(("/api/movies/titles/" + this.state.selectedMovieTitle), {
            headers: new Headers({
                'Content-Type': 'application/json'
            })
        })
        .then((response) => {
            if (response.ok) {
                return response.json();
            } else {
                console.log(response.status + " " + response.statusText);
                throw new Error("Something went wrong while fetching movie entries!");
            }
        })
        .then((selectedMovieEntries) => {
            console.log("Fetched movie entries.");
            this.setState({
                selectedMovieEntries: selectedMovieEntries
            });
        })
        .catch((error) => {
            this.handleErrors(error);
        });
    }

    handleErrors(error) {
        this.setState({
            error: error
        });
    }

    handleMovieTitleChange(event) {
        this.setState({
            selectedMovieTitle: event.target.value
        });
    }

    handleSortingOrderChange(event) {
        this.setState({
            sortingOrder: event.target.value
        });
    }

    handleInformationCardClick(clickedMovie) {
        this.setState({
            selectedMovieTitle: clickedMovie.TitleId
        });
    }

    handleRefresh() {
        this.fetchMovieTitles();
    }
}
export default MainMoviePanel;