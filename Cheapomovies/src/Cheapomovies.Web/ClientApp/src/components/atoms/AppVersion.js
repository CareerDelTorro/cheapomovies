﻿import React from 'react';

import './../../css/AppVersion.css';

/*
 * Display the version of the app.
 */
class AppVersion extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            appVersion: ""
        }
    }

    render() {
        return (
            <span className="version-text">
                {this.state.appVersion}
            </span>
        );
    }

    componentDidMount() {
        this.fetchVersion();
    }

    // retrieve version number from the manifest file
    fetchVersion() {
        fetch("/manifest.json")
        .then((response) => {
            return response.json();
        })
        .then((json) => {
            this.setState({
                appVersion: json.version
            });
        });
    }
}
export default AppVersion;