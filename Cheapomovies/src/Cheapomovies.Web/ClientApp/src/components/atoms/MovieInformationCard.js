﻿import React from 'react';

import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Card from 'react-bootstrap/Card';

import './../../css/MovieInformationCard.css';
import notFoundImage from './../../assets/image_not_found.png';

/**
 * Movie information card with elements like poster image, 
 * year, actors, plot summary, etc.
 * 
 * NB: this class has an unconventional implementation of 
 * the state update (not lifted to the parent component), 
 * because the state is supplied from two different sources: 
 * the movie object props supplied by the parent component 
 * and the onError function triggered from the HTML <img> element.
 */
class MovieInformationCard extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            posterImageSource: this.props.movieObject.Poster
        };
        this.handlePosterImageSourceError = this.handlePosterImageSourceError.bind(this);
    }

    render() {
        const movie = this.props.movieObject;
        return (
            <Card onClick={this.props.onClick} clickable={this.props.clickable}>
                <Row className="no-gutters">
                    <Col>
                        <Card.Img
                            className={"not-found-image"}
                            src={this.state.posterImageSource}
                            onError={ this.handlePosterImageSourceError }
                        />
                    </Col>
                    <Col>
                        <Card.Body>
                            <Card.Title><b>{movie.TitleId}</b></Card.Title>
                            <Card.Subtitle className={"mb-2"}><i>Year: {movie.Year}</i></Card.Subtitle>
                            <Card.Text>{movie.Plot}</Card.Text>
                        </Card.Body>
                    </Col>
                </Row>
            </Card>
        );
    }

    componentDidUpdate(prevProps) {
        // update the state if different movie object props were supplied
        if (this.props.movieObject.TitleId !== prevProps.movieObject.TitleId) {
            this.setState({
                posterImageSource: this.props.movieObject.Poster
            });
        }
    }

    // set a placeholder image in case the original poster is unavailable
    handlePosterImageSourceError() {
        this.setState({
            posterImageSource: notFoundImage
        });
    }
}
export default MovieInformationCard;