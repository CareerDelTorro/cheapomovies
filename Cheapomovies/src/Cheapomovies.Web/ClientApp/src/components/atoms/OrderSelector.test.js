﻿import React from 'react';
import ReactDOM from 'react-dom';
import OrderSelector from './OrderSelector';

it('renders without crashing', () => {
    const div = document.createElement('div');
    ReactDOM.render(<OrderSelector />, div);
});