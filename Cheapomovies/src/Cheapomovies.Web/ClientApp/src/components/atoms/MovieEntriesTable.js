﻿import React from 'react';

import Table from 'react-bootstrap/Table';

import './../../css/MovieEntriesTable.css';

/*
 * Displays a table for a movie title with multiple entries for different providers.
 */
class MovieEntriesTable extends React.Component {
    render() {
        // clone the array (can't directly modify state values)
        const movieEntries = [...this.props.selectedMovieEntries];
        // sortingOrder: 1 for ascending, -1 for descending
        movieEntries.sort((a, b) => this.props.sortingOrder * (a.Price - b.Price));
        const movieEntryRowsLoop = movieEntries.length > 0
            ? (movieEntries.map((movieEntry) =>
                <tr key={movieEntry.Provider} className={"movie-entries-table-body-row"}>
                    <td className="movie-entries-table-column-provider">
                        {movieEntry.Provider}
                    </td>
                    <td className="movie-entries-table-column-price">
                        {movieEntry.Price}
                    </td>
                </tr>)
            ) : (<h5 className="text-center">Nobody is offering this movie! :(</h5>);
        

        return (
            <>
                <Table hover className="movie-entries-table">
                    <thead>
                        <tr>
                            <th className="movie-entries-table-column-provider">Movie Provider</th>
                            <th className="movie-entries-table-column-price">Price</th>
                        </tr>
                    </thead>
                    <tbody>
                        {movieEntryRowsLoop}
                    </tbody>
                </Table>
            </>
        );
    }
}
export default MovieEntriesTable;