﻿import React from 'react';

import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Form from 'react-bootstrap/Form';

/*
 * Selector component that allows you to choose a movie.
 */
class MovieSelector extends React.Component {
    render() {
        return (
            <Form>
                <Form.Group controlId="movieSelect"> { /* controlId required for accessibility */ }
                    <Form.Label column={true} className="text-center">
                        <h5><b>Please select a movie! :)</b></h5>
                    </Form.Label>
                    <Row>
                        <Col xs="auto">
                            <Form.Control as="select" onChange={this.props.onMovieTitleChange} value={this.props.selectedMovieTitle}>
                                <option key="default-option" hidden>None</option>
                                {this.props.availableMovieTitles.map((movieTitle) =>
                                    <option key={movieTitle}>{movieTitle}</option>
                                )}
                            </Form.Control>
                        </Col>
                    </Row>
                </Form.Group>
            </Form>
        );
    }
}
export default MovieSelector;