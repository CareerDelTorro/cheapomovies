﻿import React from 'react';
import ReactDOM from 'react-dom';
import MovieInformationCard from './MovieInformationCard';

it('renders without crashing', () => {
    const div = document.createElement('div');
    ReactDOM.render(<MovieInformationCard movieObject={{ Poster: "" }}/>, div);
});