﻿import React from 'react';
import ReactDOM from 'react-dom';
import MovieSelector from './MovieSelector';

it('renders without crashing', () => {
    const div = document.createElement('div');
    ReactDOM.render(<MovieSelector availableMovieTitles={[]}/>, div);
});