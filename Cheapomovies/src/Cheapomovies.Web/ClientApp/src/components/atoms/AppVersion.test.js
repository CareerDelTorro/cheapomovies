﻿import React from 'react';
import ReactDOM from 'react-dom';
import AppVersion from './AppVersion';

it('renders without crashing', () => {
    const div = document.createElement('div');
    ReactDOM.render(<AppVersion />, div);
});
