﻿import React from 'react';

import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Form from 'react-bootstrap/Form';

/*
 * Selector component that allows you to choose sorting order (eg ascending/descending).
 */
class OrderSelector extends React.Component {
    render() {
        return (
            <Form>
                <Form.Group controlId="sortingOrderSelect"> { /* controlId required for accessibility */}
                    <Form.Label column={true} className="text-center">
                        <h5><b>Sort by price:</b></h5>
                    </Form.Label>
                    <Row>
                        <Col xs="auto">
                            <Form.Control as="select" onChange={this.props.onSortingOrderChange} value={this.props.sortingOrder}>
                                <option key="asc" value={1}>Low-to-High &#8593;</option>
                                <option key="desc" value={-1}>High-to-Low &#8595;</option>
                            </Form.Control>
                        </Col>
                    </Row>
                </Form.Group>
            </Form>
        );
    }
}
export default OrderSelector;