import React from 'react';

import AppVersion from './components/atoms/AppVersion';
import MainMoviePanel from './components/organisms/MainMoviePanel';

import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';

import './App.css';

import icon from "./assets/cheapomovies_icon.png";
import logo from "./assets/cheapomovies_logo.gif";

/*
 * Entry point of the app and the main layout of the page.
 */ 
class App extends React.Component {

    render() {
        return (
            <>
                <Container>
                    <Row className="justify-content-between align-items-center">
                        <Col xs={2}>
                            <AppVersion />
                        </Col>
                        <Col xs="auto" className="text-center">
                            <a className="btn logo-banner" href="/">
                                <img src={icon} alt="Cheap-O-Movies icon"/>
                                <img src={logo} alt="Cheap-O-Movies logo"/>
                            </a>
                        </Col>
                        <Col xs={2}>
                        </Col>
                    </Row>
                    <hr />
                </Container>
                <Container className="movie-information-container">
                    <Row>
                        <Col>
                            <MainMoviePanel />
                        </Col>
                    </Row>
                </Container>
                <Container>
                    <hr />
                    <Row className="my-3 align-items-center">
                        <Col>
                            <footer className="text-center">
                                <i><b>Disclaimer :O !</b> To save your invaluable time, some of the prices displayed might be slightly delayed. <br/>
                                   Please check the provider's site for the most up-to-date information.</i>
                            </footer>
                        </Col>
                        <Col xs="auto" className="ml-auto">
                            <footer>
                                <h5>Created by Sergey Germogentov</h5>
                            </footer>
                        </Col>
                    </Row>
                </Container>
            </>
        );
    }
}
export default App;