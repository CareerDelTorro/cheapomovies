using Cheapomovies.Core.Services;
using Cheapomovies.Data;
using Cheapomovies.Harvesting;
using Cheapomovies.Harvesting.CinemaWorld;
using Cheapomovies.Harvesting.FilmWorld;

using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SpaServices.ReactDevelopmentServer;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Newtonsoft.Json;
using Pomelo.EntityFrameworkCore.MySql.Infrastructure;
using Serilog;
using System;

namespace Cheapomovies
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            // configures Serilog to take charge of internal .NET Core logging
            services.AddLogging(loggingBuilder =>
                loggingBuilder.AddSerilog(dispose: true));

            // add database context
            services.AddDbContextPool<CheapomoviesDbContext>(options =>
            {
                // entity framework db migration doesn't seem to read environment variables properly,
                // hence the need to supply connection string from appsettings (in development environment)
                // if the connection string is read as "null"
                string connectionString = Environment.GetEnvironmentVariable("MYSQL_CONNECTION_STRING")
                    ?? Configuration.GetConnectionString("CheapomoviesDbMySqlConnection");

                options.UseMySql(connectionString, mySqlOptions =>
                {
                    mySqlOptions.ServerVersion(new Version(8, 0, 17), ServerType.MySql); // replace with your Server Version and Type
                });
            });

            // add hosted (background) services
            services.AddHostedService<ExternalApiHarvesterHostedService>();
            // add harvester services
            services.AddSingleton<IExternalApiHarvester, FilmWorldHarvester>();
            services.AddSingleton<IExternalApiHarvester, CinemaWorldHarvester>();
            // add http clients
            services.AddHttpClient<IFilmWorldHttpClient, FilmWorldHttpClient>();
            services.AddHttpClient<ICinemaWorldHttpClient, CinemaWorldHttpClient>();
            // add JSON serializer
            services.AddSingleton<JsonSerializer>();
            // add core services
            services.AddTransient<IMovieService, MovieService>();

            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);

            // In production, the React files will be served from this directory
            services.AddSpaStaticFiles(configuration =>
            {
                configuration.RootPath = "ClientApp/build";
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, Microsoft.AspNetCore.Hosting.IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Error");
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseStaticFiles();
            app.UseSpaStaticFiles();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller}/{action=Index}/{id?}");
            });

            app.UseSpa(spa =>
            {
                spa.Options.SourcePath = "ClientApp";

                if (env.IsDevelopment())
                {
                    spa.UseReactDevelopmentServer(npmScript: "start");
                }
            });
        }
    }
}
