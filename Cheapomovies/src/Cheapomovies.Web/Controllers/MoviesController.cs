using System;
using System.Collections.Generic;
using System.Threading.Tasks;

using Cheapomovies.Core.Services;
using Cheapomovies.Entities.Models.Aggregated;

using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Serilog;

namespace Cheapomovies.Controllers
{
    /// <summary>
    /// Provides endpoints for accessing movies data.
    /// </summary>
    [Route("api/[controller]")]
    public class MoviesController : ControllerBase
    {
        private readonly IMovieService _movieService;

        public MoviesController(IMovieService movieService)
        {
            _movieService = movieService;
        }

        /// <summary>
        /// Returns a list of all movie titles avaialable.
        /// </summary>
        [HttpGet("titles")]
        [ProducesResponseType(200)]
        [ProducesResponseType(500)]
        public async Task<ActionResult<List<string>>> GetMovieTitlesAsync()
        {
            try
            {
                return await _movieService.GetMovieTitleIdsAsync();
            }
            catch (Exception ex)
            {
                Log.Error($"Movie titles retrieval failed due to internal error: {ex.Message} : {ex.StackTrace}");
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        /// <summary>
        /// Returns aggregated movie entries with a given title (for all different existing providers).
        /// </summary>
        [HttpGet("titles/{title}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(500)]
        public async Task<ActionResult<List<AggregatedMovie>>> GetAggregatedMoviesAsync(string title)
        {
            try
            {
                return await _movieService.GetAggregatedMoviesByTitleAsync(title);
            }
            catch (Exception ex)
            {
                Log.Error($"Aggregate movies entries retrieval failed due to internal error: {ex.Message} : {ex.StackTrace}");
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }
    }
}
