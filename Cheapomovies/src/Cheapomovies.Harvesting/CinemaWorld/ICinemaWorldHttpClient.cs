﻿using System.Collections.Generic;
using System.Threading.Tasks;

using Cheapomovies.Entities.Models.CinemaWorld;

namespace Cheapomovies.Harvesting.CinemaWorld
{
    public interface ICinemaWorldHttpClient
    {
        Task<IEnumerable<CinemaWorldMovieShort>> GetAllMoviesAsync();

        Task<CinemaWorldMovieFull> GetMovieByIdAsync(string movieId);
    }
}
