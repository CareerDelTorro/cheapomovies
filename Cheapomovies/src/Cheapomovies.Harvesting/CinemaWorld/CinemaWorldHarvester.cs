﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;

using Cheapomovies.Data;
using Cheapomovies.Entities.Models.Aggregated;
using Cheapomovies.Entities.Models.CinemaWorld;

using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Serilog;

namespace Cheapomovies.Harvesting.CinemaWorld
{
    /// <summary>
    /// In its current implementation, the harvester retrieves the list of
    /// available movies for CinemaWorld provider and then for each movie entry:
    /// 1) retrieves individual movie information from the provider
    /// 2) stores this information in aggregated movie model
    /// 3) stores this information in mirror fashion for possible future use
    /// </summary>
    public class CinemaWorldHarvester : IExternalApiHarvester
    {
        private readonly IServiceScopeFactory _serviceScopeFactory;
        private readonly ICinemaWorldHttpClient _cinemaWorldHttpClient;

        public CinemaWorldHarvester(IServiceScopeFactory serviceScopeFactory, ICinemaWorldHttpClient cinemaWorldHttpClient)
        {
            _serviceScopeFactory = serviceScopeFactory;
            _cinemaWorldHttpClient = cinemaWorldHttpClient;
        }

        public async Task HarvestProcessAndStoreAsync()
        {
            Log.Information("CinemaWorld Harvester: STARTED harvesting.");

            try
            {
                // get a list of all movies for the provider
                IEnumerable<CinemaWorldMovieShort> movies = await _cinemaWorldHttpClient.GetAllMoviesAsync();
                Log.Information("CinemaWorld Harvester: retrieved CinemaWorld movies.");

                using (IServiceScope scope = _serviceScopeFactory.CreateScope())
                {
                    CheapomoviesDbContext dbContextScoped = scope.ServiceProvider.GetRequiredService<CheapomoviesDbContext>();

                    // retrieve individual information about each movie and store it in the database
                    foreach (CinemaWorldMovieShort movieShort in movies)
                    {
                        try
                        {
                            // retrieve live full movie description
                            //
                            CinemaWorldMovieFull newMovieFull = await _cinemaWorldHttpClient.GetMovieByIdAsync(movieShort.Id);
                            Log.Information($"CinemaWorld Harvester: retrieved {newMovieFull.Id}.");

                            // store as an aggregated movie
                            //
                            AggregatedMovie newAggregatedMovie = new AggregatedMovie
                            {
                                TitleId = newMovieFull.Title,
                                Provider = "CinemaWorld",
                                Price = newMovieFull.Price,
                                Year = newMovieFull.Year,
                                Plot = newMovieFull.Plot,
                                Poster = newMovieFull.Poster
                            };

                            AggregatedMovie storedAggregatedMovie = await dbContextScoped.FindAsync<AggregatedMovie>(newMovieFull.Title, "CinemaWorld");
                            if (storedAggregatedMovie == null)
                            {
                                await dbContextScoped.AddAsync(newAggregatedMovie);
                                Log.Information($"CinemaWorld Harvester: stored aggregated {movieShort.Title}.");
                            }
                            else
                            {
                                // detach state tracker from old entity
                                dbContextScoped.Entry(storedAggregatedMovie).State = EntityState.Detached;
                                // mark all properties of the new object as modified
                                dbContextScoped.Update(newAggregatedMovie);
                                Log.Information($"CinemaWorld Harvester: updated aggregated {movieShort.Title}.");
                            }
                            await dbContextScoped.SaveChangesAsync();

                            // store identically as retrieved
                            //
                            CinemaWorldMovieFull storedMovieFull = await dbContextScoped.FindAsync<CinemaWorldMovieFull>(movieShort.Id);
                            if (storedMovieFull == null)
                            {
                                await dbContextScoped.AddAsync(newMovieFull);
                                Log.Information($"CinemaWorld Harvester: stored {movieShort.Id} in the database.");
                            }
                            else
                            {
                                // detach state tracker from old entity
                                dbContextScoped.Entry(storedMovieFull).State = EntityState.Detached;
                                // mark all properties of the new object as modified
                                dbContextScoped.Update(newMovieFull);
                                Log.Information($"CinemaWorld Harvester: updated {movieShort.Id} in the database.");
                            }
                            await dbContextScoped.SaveChangesAsync();
                        }
                        catch (HttpRequestException ex)
                        {
                            Log.Warning($"CinemaWorld Harvester couldn't retrieve individual movie {movieShort.Id}: {ex.Message}");
                        }
                        catch (DbUpdateException ex)
                        {
                            Log.Error($"Error while saving changes to the database: {ex.Message}");
                        }
                    }
                }
            }
            catch (HttpRequestException ex)
            {
                Log.Warning($"CinemaWorld Harvester couldn't retrieve list of movies: {ex.Message}");
            }
            catch (Exception ex)
            {
                Log.Error($"CinemaWorld Harvester - something went wrong: {ex.Message} : {ex.StackTrace}");
                throw;
            }

            Log.Information("CinemaWorld Harvester: FINISHED harvesting.");
        }
    }
}
