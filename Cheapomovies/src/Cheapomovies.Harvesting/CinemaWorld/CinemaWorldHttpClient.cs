﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Http;
using System.Threading.Tasks;

using Cheapomovies.Entities.Models.CinemaWorld;

using Newtonsoft.Json;
using Serilog;

namespace Cheapomovies.Harvesting.CinemaWorld
{
    /// <summary>
    /// HTTP Client to service CinemaWorldHarvester.
    /// </summary>
    public class CinemaWorldHttpClient : ICinemaWorldHttpClient
    {
        private readonly HttpClient _httpClient;
        private readonly JsonSerializer _jsonSerializer;

        public CinemaWorldHttpClient(HttpClient httpClient, JsonSerializer jsonSerializer)
        {
            httpClient.BaseAddress = new Uri("http://webjetapitest.azurewebsites.net/api/cinemaworld/");
            httpClient.DefaultRequestHeaders.Add("Accept", "application/json");
            httpClient.DefaultRequestHeaders.Add("x-access-token",
                Environment.GetEnvironmentVariable("CINEMAWORLD_API_ACCESS_TOKEN"));

            _httpClient = httpClient;

            jsonSerializer.Error += (sender, args) =>
            {
                Log.Error(args.ErrorContext.Error.Message);
                // mark error as handled to proceed with deserialization
                args.ErrorContext.Handled = true;
            };

            _jsonSerializer = jsonSerializer;
        }

        /// <summary>
        /// Returns a list of all movies.
        /// </summary>
        public async Task<IEnumerable<CinemaWorldMovieShort>> GetAllMoviesAsync()
        {
            HttpRequestMessage request = new HttpRequestMessage
            {
                Method = HttpMethod.Get,
                RequestUri = new Uri("movies", UriKind.Relative)
            };

            HttpResponseMessage response = await _httpClient.SendAsync(request);

            if (response.IsSuccessStatusCode)
            {
                using (Stream responseStream = await response.Content.ReadAsStreamAsync())
                {
                    using (StreamReader streamReader = new StreamReader(responseStream))
                    using (JsonTextReader jsonTextReader = new JsonTextReader(streamReader))
                    {
                        CinemaWorldMovieRoot movieCollection = _jsonSerializer.Deserialize<CinemaWorldMovieRoot>(jsonTextReader);
                        return movieCollection.Movies;
                    }
                }
            }
            else
            {
                throw new HttpRequestException($"CinemaWorld HTTP request failed with the status code: {response.StatusCode.ToString()}");
            }
        }

        /// <summary>
        /// Returns a single movie found by its provider ID.
        /// </summary>
        public async Task<CinemaWorldMovieFull> GetMovieByIdAsync(string id)
        {
            HttpRequestMessage request = new HttpRequestMessage
            {
                Method = HttpMethod.Get,
                RequestUri = new Uri($"movie/{id}", UriKind.Relative)
            };

            HttpResponseMessage response = await _httpClient.SendAsync(request);

            if (response.IsSuccessStatusCode)
            {
                using (Stream responseStream = await response.Content.ReadAsStreamAsync())
                {
                    using (StreamReader streamReader = new StreamReader(responseStream))
                    using (JsonTextReader jsonTextReader = new JsonTextReader(streamReader))
                    {
                        CinemaWorldMovieFull movieFull = _jsonSerializer.Deserialize<CinemaWorldMovieFull>(jsonTextReader);
                        return movieFull;
                    }
                }
            }
            else
            {
                throw new HttpRequestException($"CinemaWorld HTTP request failed with the status code: {response.StatusCode.ToString()}");
            }
        }
    }
}
