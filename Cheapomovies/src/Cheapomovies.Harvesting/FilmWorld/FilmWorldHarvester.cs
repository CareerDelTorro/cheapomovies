﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;

using Cheapomovies.Data;
using Cheapomovies.Entities.Models.Aggregated;
using Cheapomovies.Entities.Models.FilmWorld;
using Cheapomovies.Harvesting.FilmWorld;

using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Serilog;

namespace Cheapomovies.Harvesting.CinemaWorld
{
    /// <summary>
    /// In its current implementation, the harvester retrieves the list of
    /// available movies for FilmWorld provider and then for each movie entry:
    /// 1) retrieves individual movie information from the provider
    /// 2) stores this information in aggregated movie model
    /// 3) stores this information in mirror fashion for possible future use
    /// </summary>
    public class FilmWorldHarvester : IExternalApiHarvester
    {
        private readonly IServiceScopeFactory _serviceScopeFactory;
        private readonly IFilmWorldHttpClient _filmWorldHttpClient;

        public FilmWorldHarvester(IServiceScopeFactory serviceScopeFactory, IFilmWorldHttpClient filmWorldHttpClient)
        {
            _serviceScopeFactory = serviceScopeFactory;
            _filmWorldHttpClient = filmWorldHttpClient;
        }

        public async Task HarvestProcessAndStoreAsync()
        {
            Log.Information("FilmWorld Harvester: STARTED harvesting.");

            try
            {
                // get a list of all movies for the provider
                IEnumerable<FilmWorldMovieShort> movies = await _filmWorldHttpClient.GetAllMoviesAsync();
                Log.Information("FilmWorld Harvester: retrieved FilmWorld movies.");

                using (IServiceScope scope = _serviceScopeFactory.CreateScope())
                {
                    CheapomoviesDbContext dbContextScoped = scope.ServiceProvider.GetRequiredService<CheapomoviesDbContext>();

                    // retrieve individual information about each movie and store it in the database
                    foreach (FilmWorldMovieShort movieShort in movies)
                    {
                        try
                        {
                            // retrieve live full movie description
                            //
                            FilmWorldMovieFull newMovieFull = await _filmWorldHttpClient.GetMovieByIdAsync(movieShort.Id);
                            Log.Information($"FilmWorld Harvester: retrieved {newMovieFull.Id}.");

                            // store as an aggregated movie
                            //
                            AggregatedMovie newAggregatedMovie = new AggregatedMovie
                            {
                                TitleId = newMovieFull.Title,
                                Provider = "FilmWorld",
                                Price = newMovieFull.Price,
                                Year = newMovieFull.Year,
                                Plot = newMovieFull.Plot,
                                Poster = newMovieFull.Poster
                            };

                            AggregatedMovie storedAggregatedMovie = await dbContextScoped.FindAsync<AggregatedMovie>(newMovieFull.Title, "FilmWorld");
                            if (storedAggregatedMovie == null)
                            {
                                await dbContextScoped.AddAsync(newAggregatedMovie);
                                Log.Information($"FilmWorld Harvester: stored aggregated {movieShort.Title}.");
                            }
                            else
                            {
                                // detach state tracker from old entity
                                dbContextScoped.Entry(storedAggregatedMovie).State = EntityState.Detached;
                                // mark all properties of the new object as modified
                                dbContextScoped.Update(newAggregatedMovie);
                                Log.Information($"FilmWorld Harvester: updated aggregated {movieShort.Title}.");
                            }
                            await dbContextScoped.SaveChangesAsync();

                            // store identically as retrieved
                            //
                            FilmWorldMovieFull storedMovieFull = await dbContextScoped.FindAsync<FilmWorldMovieFull>(movieShort.Id);
                            if (storedMovieFull == null)
                            {
                                await dbContextScoped.AddAsync(newMovieFull);
                                Log.Information($"FilmWorld Harvester: stored {movieShort.Id} in the database.");
                            }
                            else
                            {
                                // detach state tracker from old entity
                                dbContextScoped.Entry(storedMovieFull).State = EntityState.Detached;
                                // mark all properties of the new object as modified
                                dbContextScoped.Update(newMovieFull);
                                Log.Information($"FilmWorld Harvester: updated {movieShort.Id} in the database.");
                            }
                            await dbContextScoped.SaveChangesAsync();
                        }
                        catch (HttpRequestException ex)
                        {
                            Log.Warning($"FilmWorld Harvester couldn't retrieve individual movie {movieShort.Id}: {ex.Message}");
                        }
                        catch (DbUpdateException ex)
                        {
                            Log.Error($"Error while saving changes to the database: {ex.Message}");
                        }
                    }
                }
            }
            catch (HttpRequestException ex)
            {
                Log.Warning($"FilmWorld Harvester couldn't retrieve list of movies: {ex.Message}");
            }
            catch (Exception ex)
            {
                Log.Error($"FilmWorld Harvester - something went wrong: {ex.Message} : {ex.StackTrace}");
                throw;
            }

            Log.Information("FilmWorld Harvester: FINISHED harvesting.");
        }
    }
}
