﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Http;
using System.Threading.Tasks;

using Cheapomovies.Entities.Models.FilmWorld;

using Newtonsoft.Json;
using Serilog;

namespace Cheapomovies.Harvesting.FilmWorld
{
    /// <summary>
    /// HTTP Client to service FilmWorldHarvester.
    /// </summary>
    public class FilmWorldHttpClient : IFilmWorldHttpClient
    {
        private readonly HttpClient _httpClient;
        private readonly JsonSerializer _jsonSerializer;

        public FilmWorldHttpClient(HttpClient httpClient, JsonSerializer jsonSerializer)
        {
            httpClient.BaseAddress = new Uri("http://webjetapitest.azurewebsites.net/api/filmworld/");
            httpClient.DefaultRequestHeaders.Add("Accept", "application/json");
            httpClient.DefaultRequestHeaders.Add("x-access-token",
                Environment.GetEnvironmentVariable("FILMWORLD_API_ACCESS_TOKEN"));

            _httpClient = httpClient;

            jsonSerializer.Error += (sender, args) =>
            {
                Log.Error(args.ErrorContext.Error.Message);
                // mark error as handled to proceed with deserialization
                args.ErrorContext.Handled = true;
            };

            _jsonSerializer = jsonSerializer;
        }

        /// <summary>
        /// Returns a list of all movies.
        /// </summary>
        public async Task<IEnumerable<FilmWorldMovieShort>> GetAllMoviesAsync()
        {
            HttpRequestMessage request = new HttpRequestMessage
            {
                Method = HttpMethod.Get,
                RequestUri = new Uri("movies", UriKind.Relative)
            };

            HttpResponseMessage response = await _httpClient.SendAsync(request);

            if (response.IsSuccessStatusCode)
            {
                using (Stream responseStream = await response.Content.ReadAsStreamAsync())
                {
                    using (StreamReader streamReader = new StreamReader(responseStream))
                    using (JsonTextReader jsonTextReader = new JsonTextReader(streamReader))
                    {
                        FilmWorldMovieRoot movieCollection = _jsonSerializer.Deserialize<FilmWorldMovieRoot>(jsonTextReader);
                        return movieCollection.Movies;
                    }
                }
            }
            else
            {
                throw new HttpRequestException($"FilmWorld HTTP request failed with the status code: {response.StatusCode.ToString()}");
            }
        }

        /// <summary>
        /// Returns a single movie found by its provider ID.
        /// </summary>
        public async Task<FilmWorldMovieFull> GetMovieByIdAsync(string id)
        {
            HttpRequestMessage request = new HttpRequestMessage
            {
                Method = HttpMethod.Get,
                RequestUri = new Uri($"movie/{id}", UriKind.Relative)
            };

            HttpResponseMessage response = await _httpClient.SendAsync(request);

            if (response.IsSuccessStatusCode)
            {
                using (Stream responseStream = await response.Content.ReadAsStreamAsync())
                {
                    using (StreamReader streamReader = new StreamReader(responseStream))
                    using (JsonTextReader jsonTextReader = new JsonTextReader(streamReader))
                    {
                        FilmWorldMovieFull movieFull = _jsonSerializer.Deserialize<FilmWorldMovieFull>(jsonTextReader);
                        return movieFull;
                    }
                }
            }
            else
            {
                throw new HttpRequestException($"FilmWorld HTTP request failed with the status code: {response.StatusCode.ToString()}");
            }
        }
    }
}