﻿using System.Collections.Generic;
using System.Threading.Tasks;

using Cheapomovies.Entities.Models.FilmWorld;

namespace Cheapomovies.Harvesting.FilmWorld
{
    public interface IFilmWorldHttpClient
    {
        Task<IEnumerable<FilmWorldMovieShort>> GetAllMoviesAsync();

        Task<FilmWorldMovieFull> GetMovieByIdAsync(string movieId);
    }
}
