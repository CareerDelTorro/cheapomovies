﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

using Microsoft.Extensions.Hosting;
using Serilog;

namespace Cheapomovies.Harvesting
{
    /// <summary>
    /// This hosted service routinely (on a timer)
    /// triggers supplied harvesters' function to 
    /// harvest, store and process data from external APIs.
    /// </summary>
    public class ExternalApiHarvesterHostedService : IHostedService, IDisposable
    {
        private readonly IEnumerable<IExternalApiHarvester> _externalApiHarvesters;
        private int executionCount = 0;
        private Timer _timer;
        // interval in seconds at which the specified function will be called
        private readonly int INTERVAL_SECONDS = 15;

        public ExternalApiHarvesterHostedService(IEnumerable<IExternalApiHarvester> externalApiHarvesters)
        {
            _externalApiHarvesters = externalApiHarvesters;
        }

        public Task StartAsync(CancellationToken stoppingToken)
        {
            Log.Information("External API Harvester Hosted Service is running.");

            _timer = new Timer(HarvestStoreAndProcessExternalApis, null, TimeSpan.Zero, TimeSpan.FromSeconds(INTERVAL_SECONDS));

            return Task.CompletedTask;
        }

        // triggers harvesting, processing storing on supplied harvesters
        private void HarvestStoreAndProcessExternalApis(object state)
        {
            executionCount++;

            Log.Information(
                "External API Harvester Hosted Service ticked: {Count}", executionCount);

            foreach (IExternalApiHarvester harvester in _externalApiHarvesters)
            {
                harvester.HarvestProcessAndStoreAsync();
            }
        }

        public Task StopAsync(CancellationToken stoppingToken)
        {
            Log.Information("External API Harvester Hosted Service is stopping.");

            _timer?.Change(Timeout.Infinite, 0);

            return Task.CompletedTask;
        }

        public void Dispose()
        {
            _timer?.Dispose();
        }
    }
}
