﻿using System.Threading.Tasks;

namespace Cheapomovies.Harvesting
{
    public interface IExternalApiHarvester
    {
        Task HarvestProcessAndStoreAsync();
    }
}
