﻿using System.Collections.Generic;
using System.Threading.Tasks;

using Cheapomovies.Entities.Models.Aggregated;

namespace Cheapomovies.Core.Services
{
    public interface IMovieService
    {
        Task<List<string>> GetMovieTitleIdsAsync();

        Task<List<AggregatedMovie>> GetAggregatedMoviesByTitleAsync(string title);
    }
}
