﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using Cheapomovies.Core.Exceptions;
using Cheapomovies.Data;
using Cheapomovies.Entities.Models.Aggregated;

using Microsoft.EntityFrameworkCore;
using Serilog;

namespace Cheapomovies.Core.Services
{
    /// <summary>
    /// This service retrieves movie-related data.
    /// </summary>
    public class MovieService : IMovieService
    {
        private readonly CheapomoviesDbContext _dbContext;

        public MovieService(CheapomoviesDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        /// <summary>
        /// Queries the database for all distinct movie titles.
        /// </summary>
        public async Task<List<string>> GetMovieTitleIdsAsync()
        {
            try
            {
                return await _dbContext.AggregatedMovie.Select(am => am.TitleId).Distinct().ToListAsync();
            }
            catch (Exception ex)
            {
                string errorMessage = $"Something went wrong while retrieving data from the database: {ex.Message} : {ex.StackTrace}";
                Log.Error(errorMessage);
                throw new DatabaseOperationException(errorMessage, ex);
            }
        }

        /// <summary>
        /// Queries the database for movie entries that match the given title.
        /// Thus the entries for all providers of given title are retrieved.
        /// </summary>
        public async Task<List<AggregatedMovie>> GetAggregatedMoviesByTitleAsync(string title)
        {
            try
            {
                return await _dbContext.AggregatedMovie.Where(am => am.TitleId.Equals(title)).ToListAsync();
            }
            catch (Exception ex)
            {
                string errorMessage = $"Something went wrong while retrieving data from the database: {ex.Message} : {ex.StackTrace}";
                Log.Error(errorMessage);
                throw new DatabaseOperationException(errorMessage, ex);
            }
        }
    }
}
