﻿using System;

namespace Cheapomovies.Core.Exceptions
{
    /// <summary>
    /// Denotes unexpected exceptions arising from performing database operations.
    /// </summary>
    public class DatabaseOperationException : Exception
    {
        public DatabaseOperationException()
        {
        }

        public DatabaseOperationException(string message)
            : base(message)
        {
        }

        public DatabaseOperationException(string message, Exception innerEx)
            : base(message, innerEx)
        {
        }
    }
}
