﻿CREATE TABLE `__EFMigrationsHistory` (
    `MigrationId` varchar(95) NOT NULL,
    `ProductVersion` varchar(32) NOT NULL,
    CONSTRAINT `PK___EFMigrationsHistory` PRIMARY KEY (`MigrationId`)
);

CREATE TABLE `AggregatedMovie` (
    `Title` varchar(255) NOT NULL,
    `Provider` longtext NULL,
    CONSTRAINT `PK_AggregatedMovie` PRIMARY KEY (`Title`)
);

CREATE TABLE `CinemaWorldMovie` (
    `Id` varchar(255) NOT NULL,
    `Type` longtext NULL,
    `Title` longtext NULL,
    `Year` int NOT NULL,
    `Price` double NOT NULL,
    `Rated` longtext NULL,
    `Released` longtext NULL,
    `Runtime` longtext NULL,
    `Genre` longtext NULL,
    `Director` longtext NULL,
    `Writer` longtext NULL,
    `Actors` longtext NULL,
    `Plot` longtext NULL,
    `Language` longtext NULL,
    `Country` longtext NULL,
    `Awards` longtext NULL,
    `Poster` longtext NULL,
    `Metascore` double NOT NULL,
    `Rating` double NOT NULL,
    `Votes` int NOT NULL,
    CONSTRAINT `PK_CinemaWorldMovie` PRIMARY KEY (`Id`)
);

INSERT INTO `__EFMigrationsHistory` (`MigrationId`, `ProductVersion`)
VALUES ('20190929145755_InitialMigration', '2.1.11-servicing-32099');

DROP TABLE `CinemaWorldMovie`;

CREATE TABLE `CinemaWorldMovieFull` (
    `Id` varchar(255) NOT NULL,
    `Type` longtext NULL,
    `Title` longtext NULL,
    `Year` int NOT NULL,
    `Price` double NOT NULL,
    `Rated` longtext NULL,
    `Released` longtext NULL,
    `Runtime` longtext NULL,
    `Genre` longtext NULL,
    `Director` longtext NULL,
    `Writer` longtext NULL,
    `Actors` longtext NULL,
    `Plot` longtext NULL,
    `Language` longtext NULL,
    `Country` longtext NULL,
    `Awards` longtext NULL,
    `Poster` longtext NULL,
    `Metascore` double NOT NULL,
    `Rating` double NOT NULL,
    `Votes` int NOT NULL,
    CONSTRAINT `PK_CinemaWorldMovieFull` PRIMARY KEY (`Id`)
);

INSERT INTO `__EFMigrationsHistory` (`MigrationId`, `ProductVersion`)
VALUES ('20191001055935_ChangeStructure', '2.1.11-servicing-32099');

