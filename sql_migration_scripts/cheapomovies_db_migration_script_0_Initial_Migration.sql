﻿  IF EXISTS(SELECT 1 FROM information_schema.tables 
  WHERE table_name = '
__EFMigrationsHistory' AND table_schema = DATABASE()) 
BEGIN
CREATE TABLE `__EFMigrationsHistory` (
    `MigrationId` varchar(150) NOT NULL,
    `ProductVersion` varchar(32) NOT NULL,
    PRIMARY KEY (`MigrationId`)
);

END;

CREATE TABLE `AggregatedMovie` (
    `Title` varchar(767) NOT NULL,
    `Provider` text NULL,
    PRIMARY KEY (`Title`)
);

CREATE TABLE `CinemaWorldMovie` (
    `Id` varchar(767) NOT NULL,
    `Type` text NULL,
    `Title` text NULL,
    `Year` int NOT NULL,
    `Price` double NOT NULL,
    `Rated` text NULL,
    `Released` text NULL,
    `Runtime` text NULL,
    `Genre` text NULL,
    `Director` text NULL,
    `Writer` text NULL,
    `Actors` text NULL,
    `Plot` text NULL,
    `Language` text NULL,
    `Country` text NULL,
    `Awards` text NULL,
    `Poster` text NULL,
    `Metascore` double NOT NULL,
    `Rating` double NOT NULL,
    `Votes` int NOT NULL,
    PRIMARY KEY (`Id`)
);

INSERT INTO `__EFMigrationsHistory` (`MigrationId`, `ProductVersion`)
VALUES ('20190929141101_InitialMigration', '2.1.11-servicing-32099');

