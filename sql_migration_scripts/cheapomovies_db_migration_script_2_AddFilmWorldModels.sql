ALTER TABLE `CinemaWorldMovieFull` MODIFY COLUMN `Votes` longtext NULL;

CREATE TABLE `FilmWorldMovieFull` (
    `Id` varchar(255) NOT NULL,
    `Type` longtext NULL,
    `Title` longtext NULL,
    `Year` int NOT NULL,
    `Price` double NOT NULL,
    `Rated` longtext NULL,
    `Released` longtext NULL,
    `Runtime` longtext NULL,
    `Genre` longtext NULL,
    `Director` longtext NULL,
    `Writer` longtext NULL,
    `Actors` longtext NULL,
    `Plot` longtext NULL,
    `Language` longtext NULL,
    `Country` longtext NULL,
    `Poster` longtext NULL,
    `Metascore` double NOT NULL,
    `Rating` double NOT NULL,
    `Votes` longtext NULL,
    CONSTRAINT `PK_FilmWorldMovieFull` PRIMARY KEY (`Id`)
);

INSERT INTO `__EFMigrationsHistory` (`MigrationId`, `ProductVersion`)
VALUES ('20191001135447_AddFilmWorldModels', '2.1.11-servicing-32099');

