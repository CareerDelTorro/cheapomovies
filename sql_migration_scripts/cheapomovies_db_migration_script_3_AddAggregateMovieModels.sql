﻿CREATE TABLE `__EFMigrationsHistory` (
    `MigrationId` varchar(95) NOT NULL,
    `ProductVersion` varchar(32) NOT NULL,
    CONSTRAINT `PK___EFMigrationsHistory` PRIMARY KEY (`MigrationId`)
);

CREATE TABLE `AggregatedMovie` (
    `Title` varchar(255) NOT NULL,
    `Provider` longtext NULL,
    CONSTRAINT `PK_AggregatedMovie` PRIMARY KEY (`Title`)
);

CREATE TABLE `CinemaWorldMovie` (
    `Id` varchar(255) NOT NULL,
    `Type` longtext NULL,
    `Title` longtext NULL,
    `Year` int NOT NULL,
    `Price` double NOT NULL,
    `Rated` longtext NULL,
    `Released` longtext NULL,
    `Runtime` longtext NULL,
    `Genre` longtext NULL,
    `Director` longtext NULL,
    `Writer` longtext NULL,
    `Actors` longtext NULL,
    `Plot` longtext NULL,
    `Language` longtext NULL,
    `Country` longtext NULL,
    `Awards` longtext NULL,
    `Poster` longtext NULL,
    `Metascore` double NOT NULL,
    `Rating` double NOT NULL,
    `Votes` int NOT NULL,
    CONSTRAINT `PK_CinemaWorldMovie` PRIMARY KEY (`Id`)
);

INSERT INTO `__EFMigrationsHistory` (`MigrationId`, `ProductVersion`)
VALUES ('20190929145755_InitialMigration', '2.1.11-servicing-32099');

DROP TABLE `CinemaWorldMovie`;

CREATE TABLE `CinemaWorldMovieFull` (
    `Id` varchar(255) NOT NULL,
    `Type` longtext NULL,
    `Title` longtext NULL,
    `Year` int NOT NULL,
    `Price` double NOT NULL,
    `Rated` longtext NULL,
    `Released` longtext NULL,
    `Runtime` longtext NULL,
    `Genre` longtext NULL,
    `Director` longtext NULL,
    `Writer` longtext NULL,
    `Actors` longtext NULL,
    `Plot` longtext NULL,
    `Language` longtext NULL,
    `Country` longtext NULL,
    `Awards` longtext NULL,
    `Poster` longtext NULL,
    `Metascore` double NOT NULL,
    `Rating` double NOT NULL,
    `Votes` int NOT NULL,
    CONSTRAINT `PK_CinemaWorldMovieFull` PRIMARY KEY (`Id`)
);

INSERT INTO `__EFMigrationsHistory` (`MigrationId`, `ProductVersion`)
VALUES ('20191001055935_ChangeStructure', '2.1.11-servicing-32099');

ALTER TABLE `CinemaWorldMovieFull` MODIFY COLUMN `Votes` longtext NULL;

CREATE TABLE `FilmWorldMovieFull` (
    `Id` varchar(255) NOT NULL,
    `Type` longtext NULL,
    `Title` longtext NULL,
    `Year` int NOT NULL,
    `Price` double NOT NULL,
    `Rated` longtext NULL,
    `Released` longtext NULL,
    `Runtime` longtext NULL,
    `Genre` longtext NULL,
    `Director` longtext NULL,
    `Writer` longtext NULL,
    `Actors` longtext NULL,
    `Plot` longtext NULL,
    `Language` longtext NULL,
    `Country` longtext NULL,
    `Poster` longtext NULL,
    `Metascore` double NOT NULL,
    `Rating` double NOT NULL,
    `Votes` longtext NULL,
    CONSTRAINT `PK_FilmWorldMovieFull` PRIMARY KEY (`Id`)
);

INSERT INTO `__EFMigrationsHistory` (`MigrationId`, `ProductVersion`)
VALUES ('20191001135447_AddFilmWorldModels', '2.1.11-servicing-32099');

DROP PROCEDURE IF EXISTS POMELO_BEFORE_DROP_PRIMARY_KEY;
CREATE PROCEDURE POMELO_BEFORE_DROP_PRIMARY_KEY(IN `SCHEMA_NAME_ARGUMENT` VARCHAR(255), IN `TABLE_NAME_ARGUMENT` VARCHAR(255))
BEGIN
	DECLARE HAS_AUTO_INCREMENT_ID TINYINT(1);
	DECLARE PRIMARY_KEY_COLUMN_NAME VARCHAR(255);
	DECLARE PRIMARY_KEY_TYPE VARCHAR(255);
	DECLARE SQL_EXP VARCHAR(1000);
	SELECT COUNT(*)
		INTO HAS_AUTO_INCREMENT_ID
		FROM `information_schema`.`COLUMNS`
		WHERE `TABLE_SCHEMA` = (SELECT IFNULL(SCHEMA_NAME_ARGUMENT, SCHEMA()))
			AND `TABLE_NAME` = TABLE_NAME_ARGUMENT
			AND `Extra` = 'auto_increment'
			AND `COLUMN_KEY` = 'PRI'
			LIMIT 1;
	IF HAS_AUTO_INCREMENT_ID THEN
		SELECT `COLUMN_TYPE`
			INTO PRIMARY_KEY_TYPE
			FROM `information_schema`.`COLUMNS`
			WHERE `TABLE_SCHEMA` = (SELECT IFNULL(SCHEMA_NAME_ARGUMENT, SCHEMA()))
				AND `TABLE_NAME` = TABLE_NAME_ARGUMENT
				AND `COLUMN_KEY` = 'PRI'
			LIMIT 1;
		SELECT `COLUMN_NAME`
			INTO PRIMARY_KEY_COLUMN_NAME
			FROM `information_schema`.`COLUMNS`
			WHERE `TABLE_SCHEMA` = (SELECT IFNULL(SCHEMA_NAME_ARGUMENT, SCHEMA()))
				AND `TABLE_NAME` = TABLE_NAME_ARGUMENT
				AND `COLUMN_KEY` = 'PRI'
			LIMIT 1;
		SET SQL_EXP = CONCAT('ALTER TABLE `', (SELECT IFNULL(SCHEMA_NAME_ARGUMENT, SCHEMA())), '`.`', TABLE_NAME_ARGUMENT, '` MODIFY COLUMN `', PRIMARY_KEY_COLUMN_NAME, '` ', PRIMARY_KEY_TYPE, ' NOT NULL;');
		SET @SQL_EXP = SQL_EXP;
		PREPARE SQL_EXP_EXECUTE FROM @SQL_EXP;
		EXECUTE SQL_EXP_EXECUTE;
		DEALLOCATE PREPARE SQL_EXP_EXECUTE;
	END IF;
END;
CALL POMELO_BEFORE_DROP_PRIMARY_KEY(NULL, 'AggregatedMovie');
DROP PROCEDURE IF EXISTS POMELO_BEFORE_DROP_PRIMARY_KEY;
ALTER TABLE `AggregatedMovie` DROP PRIMARY KEY;

ALTER TABLE `AggregatedMovie` DROP COLUMN `Title`;

ALTER TABLE `AggregatedMovie` MODIFY COLUMN `Provider` varchar(255) NOT NULL;

ALTER TABLE `AggregatedMovie` ADD `TitleId` varchar(255) NOT NULL DEFAULT '';

ALTER TABLE `AggregatedMovie` ADD `Price` double NOT NULL DEFAULT 0;

ALTER TABLE `AggregatedMovie` ADD CONSTRAINT `AK_AggregatedMovie_Provider_TitleId` UNIQUE (`Provider`, `TitleId`);

ALTER TABLE `AggregatedMovie` ADD CONSTRAINT `PK_AggregatedMovie` PRIMARY KEY (`TitleId`, `Provider`);

INSERT INTO `__EFMigrationsHistory` (`MigrationId`, `ProductVersion`)
VALUES ('20191001174444_AddAggregateMovieModels', '2.1.11-servicing-32099');

