﻿ALTER TABLE `AggregatedMovie` ADD `Poster` longtext NULL;

INSERT INTO `__EFMigrationsHistory` (`MigrationId`, `ProductVersion`)
VALUES ('20191005033313_AddAggregateMoviePoster', '2.1.11-servicing-32099');

