-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: mysql
-- Generation Time: Oct 20, 2019 at 11:44 AM
-- Server version: 8.0.17
-- PHP Version: 7.2.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `cheapomovies_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `AggregatedMovie`
--

CREATE TABLE `AggregatedMovie` (
  `TitleId` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `Provider` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `Price` double DEFAULT NULL,
  `Plot` longtext,
  `Year` int(11) NOT NULL DEFAULT '0',
  `Poster` longtext
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Table structure for table `CinemaWorldMovieFull`
--

CREATE TABLE `CinemaWorldMovieFull` (
  `Id` varchar(255) NOT NULL,
  `Type` longtext,
  `Title` longtext,
  `Year` int(11) NOT NULL,
  `Price` double NOT NULL,
  `Rated` longtext,
  `Released` longtext,
  `Runtime` longtext,
  `Genre` longtext,
  `Director` longtext,
  `Writer` longtext,
  `Actors` longtext,
  `Plot` longtext,
  `Language` longtext,
  `Country` longtext,
  `Awards` longtext,
  `Poster` longtext,
  `Metascore` double NOT NULL,
  `Rating` double NOT NULL,
  `Votes` longtext
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Table structure for table `FilmWorldMovieFull`
--

CREATE TABLE `FilmWorldMovieFull` (
  `Id` varchar(255) NOT NULL,
  `Type` longtext,
  `Title` longtext,
  `Year` int(11) NOT NULL,
  `Price` double NOT NULL,
  `Rated` longtext,
  `Released` longtext,
  `Runtime` longtext,
  `Genre` longtext,
  `Director` longtext,
  `Writer` longtext,
  `Actors` longtext,
  `Plot` longtext,
  `Language` longtext,
  `Country` longtext,
  `Poster` longtext,
  `Metascore` double NOT NULL,
  `Rating` double NOT NULL,
  `Votes` longtext
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Table structure for table `__EFMigrationsHistory`
--

CREATE TABLE `__EFMigrationsHistory` (
  `MigrationId` varchar(95) NOT NULL,
  `ProductVersion` varchar(32) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `__EFMigrationsHistory`
--

INSERT INTO `__EFMigrationsHistory` (`MigrationId`, `ProductVersion`) VALUES
('20190929145755_InitialMigration', '2.1.11-servicing-32099'),
('20191001055935_ChangeStructure', '2.1.11-servicing-32099'),
('20191001135447_AddFilmWorldModels', '2.1.11-servicing-32099'),
('20191005031416_AddAggregateMovieProperties', '2.1.11-servicing-32099'),
('20191005033313_AddAggregateMoviePoster', '2.1.11-servicing-32099');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `AggregatedMovie`
--
ALTER TABLE `AggregatedMovie`
  ADD PRIMARY KEY (`TitleId`,`Provider`);

--
-- Indexes for table `CinemaWorldMovieFull`
--
ALTER TABLE `CinemaWorldMovieFull`
  ADD PRIMARY KEY (`Id`);

--
-- Indexes for table `FilmWorldMovieFull`
--
ALTER TABLE `FilmWorldMovieFull`
  ADD PRIMARY KEY (`Id`);

--
-- Indexes for table `__EFMigrationsHistory`
--
ALTER TABLE `__EFMigrationsHistory`
  ADD PRIMARY KEY (`MigrationId`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
