-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: mysql
-- Generation Time: Oct 01, 2019 at 06:15 PM
-- Server version: 8.0.17
-- PHP Version: 7.2.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `cheapomovies_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `AggregatedMovie`
--

CREATE TABLE `AggregatedMovie` (
  `TitleId` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `Provider` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `Price` double DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `AggregatedMovie`
--

INSERT INTO `AggregatedMovie` (`TitleId`, `Provider`, `Price`) VALUES
('Star Wars: Episode I - The Phantom Menace', 'CinemaWorld', 123343.5),
('Star Wars: Episode I - The Phantom Menace', 'FilmWorld', 900.5),
('Star Wars: Episode II - Attack of the Clones', 'CinemaWorld', 12.5),
('Star Wars: Episode II - Attack of the Clones', 'FilmWorld', 1249.5),
('Star Wars: Episode III - Revenge of the Sith', 'CinemaWorld', 125.5),
('Star Wars: Episode III - Revenge of the Sith', 'FilmWorld', 129.9),
('Star Wars: Episode IV - A New Hope', 'CinemaWorld', 123.5),
('Star Wars: Episode IV - A New Hope', 'FilmWorld', 29.5),
('Star Wars: Episode V - The Empire Strikes Back', 'CinemaWorld', 13.5),
('Star Wars: Episode V - The Empire Strikes Back', 'FilmWorld', 1295),
('Star Wars: Episode VI - Return of the Jedi', 'CinemaWorld', 253.5),
('Star Wars: Episode VI - Return of the Jedi', 'FilmWorld', 69.5),
('Star Wars: The Force Awakens', 'CinemaWorld', 129.5);

-- --------------------------------------------------------

--
-- Table structure for table `CinemaWorldMovieFull`
--

CREATE TABLE `CinemaWorldMovieFull` (
  `Id` varchar(255) NOT NULL,
  `Type` longtext,
  `Title` longtext,
  `Year` int(11) NOT NULL,
  `Price` double NOT NULL,
  `Rated` longtext,
  `Released` longtext,
  `Runtime` longtext,
  `Genre` longtext,
  `Director` longtext,
  `Writer` longtext,
  `Actors` longtext,
  `Plot` longtext,
  `Language` longtext,
  `Country` longtext,
  `Awards` longtext,
  `Poster` longtext,
  `Metascore` double NOT NULL,
  `Rating` double NOT NULL,
  `Votes` longtext
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `CinemaWorldMovieFull`
--

INSERT INTO `CinemaWorldMovieFull` (`Id`, `Type`, `Title`, `Year`, `Price`, `Rated`, `Released`, `Runtime`, `Genre`, `Director`, `Writer`, `Actors`, `Plot`, `Language`, `Country`, `Awards`, `Poster`, `Metascore`, `Rating`, `Votes`) VALUES
('cw0076759', 'movie', 'Star Wars: Episode IV - A New Hope', 1977, 123.5, 'PG', '25 May 1977', '121 min', 'Action, Adventure, Fantasy', 'George Lucas', 'George Lucas', 'Mark Hamill, Harrison Ford, Carrie Fisher, Peter Cushing', 'Luke Skywalker joins forces with a Jedi Knight, a cocky pilot, a wookiee and two droids to save the galaxy from the Empire\'s world-destroying battle-station, while also attempting to rescue Princess Leia from the evil Darth Vader.', 'English', 'USA', 'Won 6 Oscars. Another 48 wins & 28 nominations.', 'http://ia.media-imdb.com/images/M/MV5BOTIyMDY2NGQtOGJjNi00OTk4LWFhMDgtYmE3M2NiYzM0YTVmXkEyXkFqcGdeQXVyNTU1NTcwOTk@._V1_SX300.jpg', 92, 8.7, '915,459'),
('cw0080684', 'movie', 'Star Wars: Episode V - The Empire Strikes Back', 1980, 13.5, 'PG', '20 Jun 1980', '124 min', 'Action, Adventure, Fantasy', 'Irvin Kershner', 'Leigh Brackett (screenplay), Lawrence Kasdan (screenplay), George Lucas (story by)', 'Mark Hamill, Harrison Ford, Carrie Fisher, Billy Dee Williams', 'After the Rebel base on the icy planet Hoth is taken over by the Empire, Han, Leia, Chewbacca, and C-3PO flee across the galaxy from the Empire. Luke travels to the forgotten planet of Dagobah to receive training from the Jedi master Yoda, while Vader endlessly pursues him.', 'English', 'USA', 'Won 1 Oscar. Another 19 wins & 18 nominations.', 'http://ia.media-imdb.com/images/M/MV5BMjE2MzQwMTgxN15BMl5BanBnXkFtZTcwMDQzNjk2OQ@@._V1_SX300.jpg', 80, 8.8, '842,451'),
('cw0086190', 'movie', 'Star Wars: Episode VI - Return of the Jedi', 1983, 253.5, 'PG', '25 May 1983', '131 min', 'Action, Adventure, Fantasy', 'Richard Marquand', 'Lawrence Kasdan (screenplay), George Lucas (screenplay), George Lucas (story by)', 'Mark Hamill, Harrison Ford, Carrie Fisher, Billy Dee Williams', 'After rescuing Han Solo from the palace of Jabba the Hutt, the rebels attempt to destroy the second Death Star, while Luke struggles to make Vader return from the dark side of the Force.', 'English', 'USA', 'Nominated for 4 Oscars. Another 18 wins & 16 nominations.', 'http://ia.media-imdb.com/images/M/MV5BMTQ0MzI1NjYwOF5BMl5BanBnXkFtZTgwODU3NDU2MTE@._V1._CR93,97,1209,1861_SX89_AL_.jpg_V1_SX300.jpg', 53, 8.4, '686,479'),
('cw0120915', 'movie', 'Star Wars: Episode I - The Phantom Menace', 1999, 123343.5, 'PG', '19 May 1999', '136 min', 'Action, Adventure, Fantasy', 'George Lucas', 'George Lucas', 'Liam Neeson, Ewan McGregor, Natalie Portman, Jake Lloyd', 'Two Jedi Knights escape a hostile blockade to find allies and come across a young boy who may bring balance to the Force, but the long dormant Sith resurface to reclaim their old glory.', 'English', 'USA', 'Nominated for 3 Oscars. Another 24 wins & 60 nominations.', 'http://ia.media-imdb.com/images/M/MV5BMTQ4NjEwNDA2Nl5BMl5BanBnXkFtZTcwNDUyNDQzNw@@._V1_SX300.jpg', 51, 6.5, '537,242'),
('cw0121765', 'movie', 'Star Wars: Episode II - Attack of the Clones', 2002, 12.5, 'PG', '16 May 2002', '142 min', 'Action, Adventure, Fantasy', 'George Lucas', 'George Lucas (screenplay), Jonathan Hales (screenplay), George Lucas (story by)', 'Ewan McGregor, Natalie Portman, Hayden Christensen, Christopher Lee', 'Ten years after initially meeting, Anakin Skywalker shares a forbidden romance with Padmé, while Obi-Wan investigates an assassination attempt on the Senator and discovers a secret clone army crafted for the Jedi.', 'English', 'USA', 'Nominated for 1 Oscar. Another 16 wins & 53 nominations.', 'https://images-na.ssl-images-amazon.com/images/M/MV5BNDRkYzA4OGYtOTBjYy00YzFiLThhYmYtMWUzMDBmMmZkM2M3XkEyXkFqcGdeQXVyNDYyMDk5MTU@._V1_SX300.jpg', 54, 6.7, '469,134'),
('cw0121766', 'movie', 'Star Wars: Episode III - Revenge of the Sith', 2005, 125.5, 'PG-13', '19 May 2005', '140 min', 'Action, Adventure, Fantasy', 'George Lucas', 'George Lucas', 'Ewan McGregor, Natalie Portman, Hayden Christensen, Ian McDiarmid', 'During the near end of the clone wars, Darth Sidious has revealed himself and is ready to execute the last part of his plan to rule the Galaxy. Sidious is ready for his new apprentice, Lord...', 'English', 'USA', 'Nominated for 1 Oscar. Another 25 wins & 51 nominations.', 'http://ia.media-imdb.com/images/M/MV5BNTc4MTc3NTQ5OF5BMl5BanBnXkFtZTcwOTg0NjI4NA@@._V1_SX300.jpg', 68, 7.6, '522,705'),
('cw2488496', 'movie', 'Star Wars: The Force Awakens', 2015, 129.5, 'PG-13', '18 Dec 2015', '138 min', 'Action, Adventure, Fantasy', 'J.J. Abrams', 'Lawrence Kasdan, J.J. Abrams, Michael Arndt, George Lucas (based on characters created by)', 'Harrison Ford, Mark Hamill, Carrie Fisher, Adam Driver', '30 years after the defeat of Darth Vader and the Empire, Rey, a scavenger from the planet Jakku, finds a BB-8 droid that knows the whereabouts of the long lost Luke Skywalker. Rey, as well as a rogue stormtrooper and two smugglers, are thrown into the middle of a battle between the Resistance and the daunting legions of the First Order.', 'English', 'USA', 'Nominated for 5 Oscars. Another 48 wins & 104 nominations.', 'http://ia.media-imdb.com/images/M/MV5BOTAzODEzNDAzMl5BMl5BanBnXkFtZTgwMDU1MTgzNzE@._V1_SX300.jpg', 81, 8.2, '575,439');

-- --------------------------------------------------------

--
-- Table structure for table `FilmWorldMovieFull`
--

CREATE TABLE `FilmWorldMovieFull` (
  `Id` varchar(255) NOT NULL,
  `Type` longtext,
  `Title` longtext,
  `Year` int(11) NOT NULL,
  `Price` double NOT NULL,
  `Rated` longtext,
  `Released` longtext,
  `Runtime` longtext,
  `Genre` longtext,
  `Director` longtext,
  `Writer` longtext,
  `Actors` longtext,
  `Plot` longtext,
  `Language` longtext,
  `Country` longtext,
  `Poster` longtext,
  `Metascore` double NOT NULL,
  `Rating` double NOT NULL,
  `Votes` longtext
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `FilmWorldMovieFull`
--

INSERT INTO `FilmWorldMovieFull` (`Id`, `Type`, `Title`, `Year`, `Price`, `Rated`, `Released`, `Runtime`, `Genre`, `Director`, `Writer`, `Actors`, `Plot`, `Language`, `Country`, `Poster`, `Metascore`, `Rating`, `Votes`) VALUES
('fw0076759', 'movie', 'Star Wars: Episode IV - A New Hope', 1977, 29.5, 'PG', '25 May 1977', '121 min', 'Action, Adventure, Fantasy', 'George Lucas', 'George Lucas', 'Mark Hamill, Harrison Ford, Carrie Fisher, Peter Cushing', 'Luke Skywalker joins forces with a Jedi Knight, a cocky pilot, a wookiee and two droids to save the galaxy from the Empire\'s world-destroying battle-station, while also attempting to rescue Princess Leia from the evil Darth Vader.', 'English', 'USA', 'http://ia.media-imdb.com/images/M/MV5BOTIyMDY2NGQtOGJjNi00OTk4LWFhMDgtYmE3M2NiYzM0YTVmXkEyXkFqcGdeQXVyNTU1NTfwOTk@._V1_SX300.jpg', 92, 8.7, '915,459'),
('fw0080684', 'movie', 'Star Wars: Episode V - The Empire Strikes Back', 1980, 1295, 'PG', '20 Jun 1980', '124 min', 'Action, Adventure, Fantasy', 'Irvin Kershner', 'Leigh Brackett (screenplay), Lawrence Kasdan (screenplay), George Lucas (story by)', 'Mark Hamill, Harrison Ford, Carrie Fisher, Billy Dee Williams', 'After the Rebel base on the icy planet Hoth is taken over by the Empire, Han, Leia, Chewbacca, and C-3PO flee across the galaxy from the Empire. Luke travels to the forgotten planet of Dagobah to receive training from the Jedi master Yoda, while Vader endlessly pursues him.', 'English', 'USA', 'http://ia.media-imdb.com/images/M/MV5BMjE2MzQwMTgxN15BMl5BanBnXkFtZTfwMDQzNjk2OQ@@._V1_SX300.jpg', 80, 8.8, '842,451'),
('fw0086190', 'movie', 'Star Wars: Episode VI - Return of the Jedi', 1983, 69.5, 'PG', '25 May 1983', '131 min', 'Action, Adventure, Fantasy', 'Richard Marquand', 'Lawrence Kasdan (screenplay), George Lucas (screenplay), George Lucas (story by)', 'Mark Hamill, Harrison Ford, Carrie Fisher, Billy Dee Williams', 'After rescuing Han Solo from the palace of Jabba the Hutt, the rebels attempt to destroy the second Death Star, while Luke struggles to make Vader return from the dark side of the Force.', 'English', 'USA', 'http://ia.media-imdb.com/images/M/MV5BMTQ0MzI1NjYwOF5BMl5BanBnXkFtZTgwODU3NDU2MTE@._V1._CR93,97,1209,1861_SX89_AL_.jpg_V1_SX300.jpg', 53, 8.4, '686,479'),
('fw0120915', 'movie', 'Star Wars: Episode I - The Phantom Menace', 1999, 900.5, 'PG', '19 May 1999', '136 min', 'Action, Adventure, Fantasy', 'George Lucas', 'George Lucas', 'Liam Neeson, Ewan McGregor, Natalie Portman, Jake Lloyd', 'Two Jedi Knights escape a hostile blockade to find allies and come across a young boy who may bring balance to the Force, but the long dormant Sith resurface to reclaim their old glory.', 'English', 'USA', 'http://ia.media-imdb.com/images/M/MV5BMTQ4NjEwNDA2Nl5BMl5BanBnXkFtZTfwNDUyNDQzNw@@._V1_SX300.jpg', 51, 6.5, '537,242'),
('fw0121765', 'movie', 'Star Wars: Episode II - Attack of the Clones', 2002, 1249.5, 'PG', '16 May 2002', '142 min', 'Action, Adventure, Fantasy', 'George Lucas', 'George Lucas (screenplay), Jonathan Hales (screenplay), George Lucas (story by)', 'Ewan McGregor, Natalie Portman, Hayden Christensen, Christopher Lee', 'Ten years after initially meeting, Anakin Skywalker shares a forbidden romance with Padmé, while Obi-Wan investigates an assassination attempt on the Senator and discovers a secret clone army crafted for the Jedi.', 'English', 'USA', 'https://images-na.ssl-images-amazon.com/images/M/MV5BNDRkYzA4OGYtOTBjYy00YzFiLThhYmYtMWUzMDBmMmZkM2M3XkEyXkFqcGdeQXVyNDYyMDk5MTU@._V1_SX300.jpg', 54, 6.7, '469,134'),
('fw0121766', 'movie', 'Star Wars: Episode III - Revenge of the Sith', 2005, 129.9, 'PG-13', '19 May 2005', '140 min', 'Action, Adventure, Fantasy', 'George Lucas', 'George Lucas', 'Ewan McGregor, Natalie Portman, Hayden Christensen, Ian McDiarmid', 'During the near end of the clone wars, Darth Sidious has revealed himself and is ready to execute the last part of his plan to rule the Galaxy. Sidious is ready for his new apprentice, Lord...', 'English', 'USA', 'http://ia.media-imdb.com/images/M/MV5BNTc4MTc3NTQ5OF5BMl5BanBnXkFtZTfwOTg0NjI4NA@@._V1_SX300.jpg', 68, 7.6, '522,705');

-- --------------------------------------------------------

--
-- Table structure for table `__EFMigrationsHistory`
--

CREATE TABLE `__EFMigrationsHistory` (
  `MigrationId` varchar(95) NOT NULL,
  `ProductVersion` varchar(32) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `__EFMigrationsHistory`
--

INSERT INTO `__EFMigrationsHistory` (`MigrationId`, `ProductVersion`) VALUES
('20190929145755_InitialMigration', '2.1.11-servicing-32099'),
('20191001055935_ChangeStructure', '2.1.11-servicing-32099'),
('20191001135447_AddFilmWorldModels', '2.1.11-servicing-32099');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `AggregatedMovie`
--
ALTER TABLE `AggregatedMovie`
  ADD PRIMARY KEY (`TitleId`,`Provider`);

--
-- Indexes for table `CinemaWorldMovieFull`
--
ALTER TABLE `CinemaWorldMovieFull`
  ADD PRIMARY KEY (`Id`);

--
-- Indexes for table `FilmWorldMovieFull`
--
ALTER TABLE `FilmWorldMovieFull`
  ADD PRIMARY KEY (`Id`);

--
-- Indexes for table `__EFMigrationsHistory`
--
ALTER TABLE `__EFMigrationsHistory`
  ADD PRIMARY KEY (`MigrationId`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
